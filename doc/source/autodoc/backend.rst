***************
Backend
***************

.. toctree::
	:maxdepth: 3

	backend/admin.rst
	backend/consumers.rst
	backend/models.rst
	backend/middlewares.rst
	backend/scheduler.rst
	backend/tests.rst
	backend/views.rst
	backend/websocket_client.rst

	backend/commands.rst
	backend/scheduling.rst
