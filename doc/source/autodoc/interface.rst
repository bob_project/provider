*****************
Interface
*****************

.. toctree::
	:maxdepth: 2

	interface/admin.rst
	interface/models.rst
	interface/tests.rst
	interface/views.rst
