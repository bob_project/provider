Run Script
----------

.. automodule:: provider_backend.management.commands.run_script
    :members:
    :undoc-members:
    :show-inheritance:
