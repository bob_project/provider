Voters
------

.. automodule:: provider_backend.scheduling.voters
    :members:
    :undoc-members:
    :show-inheritance:
