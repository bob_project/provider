Websocket Client
-----------------

.. automodule:: provider_backend.websocket_client
    :members:
    :undoc-members:
    :show-inheritance:
