Coordinator
#############

This application was created to provide a reliable environment to benchmark our solution. Our goal was to evaluate the efficiency of shadow banning through different cases. To do so, we implemented three models: ``Batch``, ``BatchWorker`` and ``Computer``. ``Batch`` and ``BatchWorker`` models aim to store collected metrics of each benchmark ran with our system. With the :ref:`autodoc/coordinator/coordinator:Coordinator` command we coordinate our computers running the script loacated at `Selenium`_. With this script, we can control registered and connected computers to create fake workers using ``Selenium``. ``Selenium`` enables us to create a thread with a browser visiting a chosen page (in our case our website).

To produce our benchmark, we chose to test every combination of ``mode_attribute``, ``mode_vote``, ``project`` (``PNG`` or ``HASH`` tasks), ``shadow`` or ``classic`` ban and different saboteur rates (``0, 0.05, 0.1, 0.2, 0.3, 0.4, 0.5``). For each combination we created a set of workers with three possible profiles :

- Short honest: a worker submitting true results and lasting 60 seconds on the website;
- Long honest: a worker submitting true results and lasting 5 minutes on the website;
- Saboteur: a worker submitting false results, lasting for a total of 5 minutes on the website and coming back after getting banned.

We distributed these profiles using the saboteur rate and a fixed rate of short workers (``0.8``) based on the Weibull distribution. Now we only had to send to each computer their run configuration (how many worker of each time). Then wait for them to launch these workers on the website and signal all workers stopped to register collected results on the system.

:ref:`autodoc/coordinator/coordinator:Coordinator` command is similar to the :ref:`autodoc/backend/commands/backend:Backend` and is also based on the :ref:`autodoc/backend/websocket_client:Websocket Client` wrapper. We added a ``BatchManager`` class which act as the ``Scheduler`` to regroup all database actions. When launching this command you can choose which choice of each variable you keep to modulate the number of combinations. At the end, you can download the generated data with a custom action on the admin panel of the ``Batch`` model.

.. _Selenium: https://gitlab.com/bob_project/orchestration
