Backend
#############

Background command
******************

:ref:`autodoc/backend:Backend` is the main part of the project. It contains all the logic described in the :ref:`general/presentation:Techical architecture`. Although the database scheme is already described in :ref:`general/presentation:Logical architecture` section, we will explain here how the different files interact together.

:ref:`autodoc/backend/commands/backend:Backend` is the entry file of all the backend processes. It's organised as a WebSocket client using the :ref:`autodoc/backend/websocket_client:Websocket Client` wrapper. This enables us to create three background threads with a dedicated role for each. Before describing these threads, we have to take a look at the ``BackendClient`` attributes. ``WebSocketClient`` has by default a ``backend`` attribute which is used to store the websocket URL, the authentication token, the ping interval of the connection, the maximum number of retry after a disconnection and a stop flag. In addition to this attribute, ``BackendClient`` takes an extra ``scheduler`` attribute which is an instance of :ref:`autodoc/backend/scheduler:Scheduler`. This attribute will be used as the entry point for all database related operations. Indeed, as the Websocket client run in ``async`` mode, we need to separate these type of operations with a ``sync_to_async`` conversion. :ref:`autodoc/backend/scheduler:Scheduler` defines three callable methods : ``attribute``, ``process`` and ``vote``. These methods will match each Job configuration (voting, attributing and processing modes) with the corresponding code and call it.

The 3 background tasks are reflecting these three methods. Each thread takes in charge one call of the :ref:`autodoc/backend/scheduler:Scheduler` methods. The ``attribute`` thread runs every X seconds and calls the appropriate :ref:`autodoc/backend/scheduling/attributors:Attributors` through the :ref:`autodoc/backend/scheduler:Scheduler` to create ``Computations``. It retrieves these computations and for each sends a ``START_COMPUTATION`` signal to the appropriate ``Worker``. Similarly the ``vote`` thread will call the appropriate :ref:`autodoc/backend/scheduling/voters:Voters` for each ``Job`` and try to create a ``Result`` for each ``Task`` of the ``Job`` using submitted ``Computations``. Finally, the ``process`` thread receives all websocket messages from workers and processes them with the appropriate :ref:`autodoc/backend/scheduling/processors:Processors` through the :ref:`autodoc/backend/scheduler:Scheduler`.

To sum up, here is a little schema:

.. image:: /static/backend.png


Websocket server
****************

The :ref:`autodoc/backend:Backend` application also contains a :ref:`autodoc/backend/consumers:Consumers` file which describes the WebSocket server. Basically, it transmits messages to worker by matching its connection session and sending all messages from workers to any connected ``BackendWorker``.

Web views
*********

In addition to the admin panel described in the :ref:`autodoc/backend/admin:Admin` files, the :ref:`autodoc/backend/views:Views` file of :ref:`autodoc/backend:Backend` contains three views:

- ``index`` (``/``): the home page of the web site
- ``worker`` (``/worker/``): the view containing the ``Javascript`` script which connect the ``Worker`` to our system
- ``result`` (``/result/<id>/``): this view retrieve results computed by the workers with ``POST`` resquests

The detail of the JS and WASM code can be found in the `Distributor`_ repository.

.. _Distributor: https://gitlab.com/bob_project/distributor
