Interface
#############

This application aims at providing every element to a scientist team to interact with our product. Hence, we can found in the ``models.py`` file three models: Team, Scientist and Project. For now, there is not much in this application but in the future we could add multiple views to allow user sign in, team creation, project registration, job following, etc. All this views would construct the user workflow from profile creation to result collecting.
