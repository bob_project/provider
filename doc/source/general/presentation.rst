*******************
Presentation
*******************

BOB is a Browser Based Volunteer Computing (BBVC) Framework. It was primarily developed to support a research project studying the impact of shadow banning in BBVC systems. You can found the associated paper HERE.

Besides the research approach, BOB aims at providing a simple way for scientists to make everyone participate on their research by providing their computing power. To do so when this project is deployed, volunteers will just have to visit the main page to be enrolled in the worker pool and participate. When they leave, computations stop and they won't have any other action to do.

Our system will look for each project registered on the database if there are tasks to compute and distribute them to all workers. When a worker finish a task, they automatically sends their result to the server and when multiple workers do the same on a specific task, a detection algorithm (i.e.: majority voting) is used to determine the good result. When validated, the chosen result is saved and any worker who voted for the wrong result gets banned.

Technical architecture
#####################

.. _technical-architecture:

In the following scheme we describe the technical architecture of the server.

.. image:: /static/architecture.png

There are 3 different technical parts on this server:

- Provider Server: this part has multiple functions :

  - Transmit messages between backend and workers
  - Register session of worker and backend instances
  - Retrieve results of computations sent by workers
  - Serve the admin panel

- Backend: this is the orchestration part which will :

  - Distribute tasks to worker
  - Receive workers messages and process them
  - Choose correct result between multiple ones submitted for a task

- Database: a simple data store accessed by the 2 other parties

There are also 2 actors:

- Worker: which is a volunteer visiting a web page containing our loading script
- Administrator: a member of our team registering jobs & tasks and retrieving results

Logical architecture
#####################

Here we have a quick view of the database structure.

.. image:: /static/database.png

A brief description of each table:

- ``BakendWorker``: An instance of backend running on server.
- ``Scientist``: A user profile representing a scientist team member
- ``Team``: A group of scientists owning one or multiple projects
- ``Project``: A data structure to store code which will be analysed by administrators before being compiled and distributed to workers
- ``Job``: A scientific project with multiple options for voting, processing and attributing.
- ``Task``: A subpart of a ``Job`` with a dataset to process.
- ``Computation``: A ``Task`` replication associated with a Worker.
- ``Result``: A ``Task`` result after multiple Computations were compared.
- ``Worker``: A volunteer doing computations when visiting our web pages.

Workflow
###############

On the ``Backend`` side, a thread will regularly go over the different ``Tasks`` available for each ``Job``. Each ``Job`` has an associated attribute mode, which will be used to match an available ``Worker`` to a ``Task``. When a task is attributed to a ``Worker``, a computation is created and a message ``START_COMPUTATION`` sent to the ``Worker`` through a WebSocket connection. When the ``Worker`` has completed their ``Task``, they will submit their result on a web view of the ``Provider Server`` with a ``POST`` request. Finally, on an other thread on the backend side, each ``Task`` will be processed. This thread checks all available computations and determine according to the the voting mode of the associated ``Job`` if a ``Result`` can be created for this ``Task``. When a ``Result`` is created, any ``Worker`` who submitted data different from the one chosen, will be banned. There is two forms of ban, ``shadow`` and ``classic`` depending of the ``Job`` configuration. A ``Job`` with ``shadow`` ban will keep submitting tasks to a shadow banned ``Worker`` but will not take into account its results. On the other hand, a ``Job`` with ``classic`` ban won't send any ``Task`` to a banned worker.


Here we describe this workflow from the point of view of a volunteer visiting a web page with our loading script.

.. mermaid::

   sequenceDiagram
	  Worker->>+Server: Connection
	  Server->>+Server: Register Worker
	  Server-->>Worker: Connected
	  loop connected
	    Backend->>+Backend: Attribute computation to worker
	    Backend->>+Server: signal START_COMPUTATION
	    Server->>+Worker: signal START_COMPUTATION
	    Worker->>+Server: Load code
	    Server-->>Worker: Return code
	    Worker->>+Server: signal WASM_INIT
	    Server->>+Backend: signal WASM_INIT
	    Backend->>+Backend: Process signal
	    Worker->>+Server: Load data
	    Server-->>Worker: Return data
	    Worker->>+Server: signal DATA_INIT
	    Server->>+Backend: signal DATA_INIT
	    Backend->>+Backend: Process signal
	    Worker->>+Worker: Compute
	    Worker->>+Server: signal COMPUTE_END
	    Server->>+Backend: signal COMPUTE_END
	    Backend->>+Backend: Process signal
	    Worker->>+Server: POST result
	    Worker->>+Server: signal RESULTS_END
	    Server->>+Backend: signal RESULTS_END
	    Backend->>+Backend: Process signal
	    Backend->>+Backend: Vote on results received
	    alt is minority in vote
	      Backend->>+Server: signal BAN_WORKER
	      Server->>+Worker: signal BAN_WORKER
	    end
	  end
