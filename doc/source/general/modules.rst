**************
Modules
**************

As described in the presentation documentation, there are 3 configurable types of module to change system behaviour : ``voter``, ``attributor``, and ``processor``. These modules can be found in ``src/provider_backend/scheduling/``, with a defined API as an ``abstractclass`` for each one. In this section, we will describe these APIs and the implementation of each existing variants of each module.

A new implementation of a module has to be registered in the settings file at ``src/provider/settings/base.py`` as follow :

.. code-block:: py

	ATTRIBUTORS = {
	    "FIFO": "provider_backend.scheduling.attributors.FIFOAttributor",
	    "SPOTCK": "provider_backend.scheduling.attributors.SpotCheckAttributor",
	}

	PROCESSORS = {
	    "SIMPLE": "provider_backend.scheduling.processors.SimpleProcessor",
	}

	VOTERS = {
	    "MAJORITY": "provider_backend.scheduling.voters.MajorityVoter",
	    "MFIRST": "provider_backend.scheduling.voters.MFirstVoter",
	    "CREDIB": "provider_backend.scheduling.voters.CredibilityVoter",
	}

When added, you need to run ``python3 manage.py makemigrations`` and ``python3 manage.py migrate`` to update models definition.

Attributors
##############

The attributor is in charge of attributing a ``Task`` to a ``Worker``. To do so, each attributor get called on its ``attribute(self, job)`` method. This method takes in parameter the ``Job`` to go through for attribution and returns a ``list`` of ``Computation``. Each variant of attributor will have to implement the following method:

.. code-block:: py

	@abstractmethod
	def attribute_to_worker(self, task, worker, already_worked_on_task, worker_computation_count):
        """ Look if a worker is available for a task and return created computation."""

This class is called by the ``attribute(job)`` method and have to tell if the ``task`` has to be attributed to the ``worker``. The variable ``already_worked_on_task`` documents if the worker already have a computation on this ``task`` and the variable ``worker_computation_count`` tells the number of computations the worker is working on (can be used if you want a worker to work only on one task at a time or multiple).

The ``Attributor`` class has also to other methods which can be also overridden :

.. code-block:: py

	def get_tasks(self, job):
        """ Retrieve all available Tasks of a Job """

This method retrieve all ``Tasks`` of the associated ``Job``. It can be overridden to change the order of precessing ``Tasks`` for example. Similarly, the method ``get_workers()`` can be used to change the order of processing ``Workers``.

Finally, the attributor class counts two other methods we do not recommand to override:

- ``create_computation(task, worker)`` which returns a ``Computation`` object created with the provided ``Task`` and ``Worker``. This method is used as utility in ``attribute_to_worker`` implementations.
- ``get_computations()`` which returns the set of computations used to compute if the ``Task`` reached its replication rate and the two variables ``already_worked_on_task`` and ``worker_computation_count``.

FIFOAttributor
***************

This attributor is pretty simple :

.. code-block:: py

	class FIFOAttributor(Attributor):
	    """ Simple FIFO task attributor. """

	    def attribute_to_worker(self, task, worker, already_worked_on_task, worker_computation_count):

	        computation = None
	        if not already_worked_on_task and not worker_computation_count:
	            computation = self.create_computation(task, worker)
	        return computation

It checks if the worker hasn't worked on the task and is not busy, and creates the computation after this simple check.

SpotCheckAttributor
*******************

This one is more complicated :

.. code-block:: py

	class SpotCheckAttributor(Attributor):
	    """ spot-check task attributor. """

	    def attribute_to_worker(self, task, worker, already_worked_on_task, worker_computation_count):

	        computation = None
	        if random.random() < task.job.spot_check_prob and not worker_computation_count:
	            spot_check_task = Task.objects.filter(job=task.job, expected_hash__isnull=False).order_by('?').first()
	            if spot_check_task:
	                computation = self.create_computation(spot_check_task, worker)
	        elif not already_worked_on_task and not worker_computation_count:
	            computation = self.create_computation(task, worker)
	        return computation

This attributor will, according to the ``Job`` ``spot_check_prob`` variable, sometimes attribute a spot-check task to a worker. (Note: Spot-check tasks is a regular tasks for which we already know the result.) They are used to check if the worker is honest or not. Here, each time a worker is getting attributed a task, there is a probability ``spot_check_prob`` of receiving a spot-check task. Otherwise, they just receive a regular task.

Processors
##############

Processors are used to react to workers' WebSocket messages. These messages can be of 4 types:

- ``WASM_INIT``: the wasm module is loaded/initialised
- ``DATA_INIT``: the data required for the computation is downloaded
- ``COMPUTE_END``: the computation is finished
- ``RESULTS_END``: the result is uploaded to the server

The ``Processor`` class defines the ``process(message)`` method which checks the content of the message and adds context variables. It then dispatches the message to the appropriated method (one for each type of message):

.. code-block:: py

	@abstractmethod
    def wasm_init(self, message, worker, computation):
        """ Process WASM_INIT messages. """

    @abstractmethod
    def data_init(self, message, worker, computation):
        """ Process DATA_INIT messages. """

    @abstractmethod
    def compute_end(self, message, worker, computation):
        """ Process COMPUTE_END messages. """

    @abstractmethod
    def result_end(self, message, worker, computation):
        """ Process RESULT_END messages. """

Each method takes in parameter:

- The message itself
- The worker who sent the message
- The computation associated with the message

These 4 methods have to be overridden by each new processor.

SimpleProcessor
***************

This processor defines a basic behaviour on message reaction:

.. code-block:: py

	def wasm_init(self, message, worker, computation):

        if worker.state != 'DOWN':
            worker.state = 'RUNNING'
            worker.save()

When a ``WASM_INIT`` message is received, we change the ``Worker`` state to ``RUNNING``

.. code-block:: py

    def data_init(self, message, worker, computation):

        if computation.state == 'TODO':
            computation.state = 'DOING'
            computation.save()
        if computation.task.state == 'TODO' and not computation.task.expected_hash:
            computation.task.state = 'DOING'
            computation.task.save()
        if computation.task.job.state == 'TODO':
            computation.task.job.state = 'DOING'
            computation.task.job.save()

When a ``DATA_INIT`` message is received we change the ``Computation``, ``Task`` and ``Job`` state to ``DOING`` if necessary.

.. code-block:: py

    def compute_end(self, message, worker, computation):
        pass

We do not react to ``COMPUTE_END`` messaged for now.

.. code-block:: py

    def result_end(self, message, worker, computation):

        if worker.banned and computation.state != 'BANNED':
            computation.state = 'BANNED'
            computation.save()
        if worker.state != 'DOWN':
            worker.state = 'UP'
            worker.save()

When a ``RESULT_END`` message is received we change the ``Worker`` state to ``UP`` and set the computation state to ``BANNED`` if necessary.

Voters
##############

A voter is in charge of determining for a group of ``Computation`` associated to a ``Task`` if a ``Result`` can be created. To do so, we define a base class ``Vote`` which implements all utility around this process. The entry point of this class is the ``vote(job)`` which does this process for each ``Task`` of the provided ``Job`` and returns a ``list`` of ``Workers`` to ban. Each implementation of a voter have to override this method:

.. code-block:: py

	@abstractmethod
    def vote_task(self, task, computations, stats):
        """ Vote for the correct hash with selected method. """

``vote_task`` takes as parameter the ``Task`` to vote for, the associated set of ``Computations`` and a dict called ``stats``. The last parameter is structured as the following:

.. code-block:: py

	{
		'data_hash_1': [computation_a, computation_b],
		'data_hash_2': [computation_c],
	}

The data hash is the unique key to discriminate every different result sent by all ``Workers`` for the associated ``Task`` and gives access to the set of ``Computation`` giving this result. It's used in every voter to determine the hash with the best popularity, credibility, ...

The ``vote_task`` returns two ``list`` :

- ``correct_hash``: The list of data hash considered as correct (can be empty or with only one element for most case). If ``correct_hash`` contains more than one hash, all computation which did not vote for one of these hash will be rerolled. If there is only one hash, a ``Result`` is created and all ``Worker`` who didn't vote for the chosen hash gets banned.
- ``cheaters``: A list of ``Workers`` considered as cheaters (in most case it's empty)

MajorityVoter
***************

This first voter is simple majority voting. It waits for each ``Task`` to reach the maximum replication number and chooses the most popular data hash in the provided set of results. ``max_nb_vote`` designates an integer corresponding to the maximum number of vote for a data hash and is used to retrieve all hashes with the maximum number of vote in ``correct_hash``.

.. code-block:: py

	class MajorityVoter(Voter):
	    """ Choose final result by voting between all computations affected to task. """

	    def vote_task(self, task, computations, stats):

	        correct_hash = []
	        # wait for all computations to be completed
	        if computations.count() >= task.job.replications:
	            # max_nb_vote = maximum number of vote for the same number
	            max_nb_vote = len(stats[max(stats, key= lambda k: len(stats[k]))])
	            correct_hash = [filtered for filtered in filter(lambda k: len(stats[k]) == max_nb_vote, stats)]
	        return correct_hash, []

MFirstVoter
***************

This voter is similar to the previous one but it only waits for a hash to reach a ``threshold`` number of votes. The first hash which reaches the ``threshold`` is considered as correct.

.. code-block:: py

	class MFirstVoter(Voter):
	    """ Choose final result by taking the first result returned m-times (m = job.threashold). """

	    def vote_task(self, task, computations, stats):

	        correct_hash = []
	        # wait for all computations to be completed
	        if computations.count() >= task.job.threshold:
	            # max_nb_vote = maximum number of vote for the same number
	            max_nb_vote = len(stats[max(stats, key= lambda k: len(stats[k]))])
	            if max_nb_vote >= task.job.threshold:
	                correct_hash = [filtered for filtered in filter(lambda k: len(stats[k]) == max_nb_vote, stats)]
	        return correct_hash, []

CredibilityVoter
****************

This voter is more complex and is based on "Sabotage-tolerance mechanisms for volunteer computing systems" by Sarmenta et al as it has two functions:

- If the task is a spot-check one, validate the result and mark as cheater ``Worker`` who didn't provide the correct answer.
- Calculate the credibility for each hash and if it reach the ``THRESHOLD_CREDIBILITY``, consider it as valid.

The spot-check part is the first ``if`` and simply goes through all computations and compares ``hash_result`` of the ``Computation`` with ``expected_hash`` of the ``Task``. The second part computes for each data hash a group credibility. Then the maximum group credibility is compared to the threshold. If the maximum reach the threshold, every data hash at maximum credibility is considered as valid.

.. code-block:: py

	class CredibilityVoter(Voter):
	    """ Voter based on worker credibility throught regular spot-checking of worker results. """

	    def vote_task(self, task, computations, stats):

	        cheaters = []
	        correct_hash = []
	        # if task is a spot-check verify provided results
	        if task.expected_hash:
	            for computation in computations:
	                # compare exptected and provided result
	                if computation.hash_result != task.expected_hash:
	                    # if comparison fail, worker get banned
	                    logger.info("Banning : {}".format(computation.worker))
	                    computation.worker.ban()
	                    cheaters.append(computation.worker)
	                else:
	                    logger.info("Worker succeed it's spot-check : {}".format(computation.worker))
	                    computation.worker.spot_check_number += 1
	                    computation.worker.save()
	                    computation.state = 'CHECKED'
	                    computation.save()
	        # start voting with credibility based system
	        elif computations.exists():
	            groups = group_construct(stats)
	            probas_sum = sum_of_probas(groups)

	            credibility = {}
	            for result_hash in groups:
	                credibility[result_hash] = get_group_cred(result_hash, groups, probas_sum)
	            max_credibility = max(credibility.values())

	            if max_credibility >= float(Variable.objects.get(name="THRESHOLD_CREDIBILITY").value):
	                correct_hash = [filtered for filtered in filter(lambda k: credibility[k] == max_credibility, credibility)]
	        return correct_hash, cheaters
