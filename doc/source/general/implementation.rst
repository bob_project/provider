***************
Implementation
***************

Most parts of our system have been developed using `Python`_ and `Django`_. These technologies were chosen for the ease of use and speed of development. We tried to keep our code as modular as possible to allow easy evolutions and module additions. We split our code in three Django applications: ``backend``, ``interface`` and ``coordinator``.

.. include:: implementation/backend.rst
.. include:: implementation/interface.rst
.. include:: implementation/coordinator.rst

.. _Python: https://www.python.org/
.. _Django: https://www.djangoproject.com/
