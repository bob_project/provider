*******************
Installation
*******************

Local deployment
#####################

If you want to run the project on your personal computer, follow this section. For production deployment, please follow the next section.

Dependency installation
*********************

Before using the provider, you need an instance of Redis running on port 6379.
To do so, you can run a docker image, like so:

.. code-block:: sh

	docker run -p 6379:6379 -d redis:5

Local environment setup
*************************

After cloning the repository, go in the root folder of the project. You will have to create a virtualenv with Python virtualenv:

.. code-block:: sh

	python -m venv venv

And activate it

.. code-block:: sh

	./venv/Script/activate # Windows
	source venv/bin/activate # Linux

Then, you can install development dependencies with:

.. code-block:: sh

	pip install -r requirements/development.txt

Launching code locally
***********************

Now, you can initialise the database by going to ``src/`` and use:

.. code-block:: sh

	python manage.py makemigrations
	python manage.py migrate
	python manage.py createsuperuser # complete form to create an admin user

And run the local server with:

.. code-block:: sh

	python manage.py runserver

.. Note::

	Don't forget to set ``DJANGO_SETTINGS_MODULE`` environment variable to ``provider.settings.development`` or to use ``--settings=provider.settings.development`` on each ``manage.py`` command.

Now, go to ``localhost:8000/admin`` and log in with the user account created before. Navigate to ``BackendWorker`` section and create a new object. Copy the generated token and save it in ``.env`` file in ``src/`` folder as follow:

.. code-block:: ini

	BACKEND_TOKEN=<token>

Now, you can open a separate terminal, activate your virtual environment and launch the backend with:

.. code-block:: sh

	python manage.py backend

.. Note::

	You can stop it with ``Ctrl+C`` or command ``python manage.py backend --stop``

Production deployment
######################

Here, we will describe the setup needed for a public exposed server. In the following section, we will assume that the installation takes place on a Debian server, please adapt the commands to your Linux distribution if needed.

Redis configuration
**********************

In this section, we will only describe the steps of `this <https://www.codeflow.site/fr/article/how-to-install-and-secure-redis-on-debian-10>`_ tutorial, you can also refer to it if needed.

Install redis-server:

.. code-block:: sh

	sudo apt update
	sudo apt install redis-server

Edit the redis configuration at ``/etc/redis/redis.conf`` and set ``supervised`` directive to ``systemd``, also set ``requirepass`` directive with a properly generated random password.

Restart your Redis:

.. code-block:: sh

	sudo service redis restart

And test it using ``redis-cli`` command.

Supervisor configuration
************************

Supervisor will be used to run our server and backend, install package with:

.. code-block:: sh

	sudo apt update
	sudo apt-get install supervisor

We will need two supervisor programs to run the server and the backend. Configuration files will be saved under ``/etc/supervisor/conf.d/``

.. code-block:: cfg

	# provider.conf
	[program:provider]
	command = bash runinenv.sh ../venv daphne -v 2 -p 8000 provider.asgi:application
	user=<your_user>
	autostart = true
	autorestart = true
	directory = <path_to_provider_repository>/srv
	stdout_logfile=/var/log/provider/console.log
	redirect_stderr=true

	# backend.conf
	[program:backend]
	command = bash runinenv.sh ../venv python3 manage.py backend
	user=<your_user>
	autostart = true
	autorestart = true
	directory = <path_to_provider_repository>/src
	stdout_logfile=/var/log/backend/console.log
	redirect_stderr=true

.. Note::

	Don't forget to check that the log folders exists before running.

You can restart supervisor with ``sudo service supervisor restart``. Now it would crash as we didn't setup the database and the environment.


Postgres configuration
************************

Install PostgreSQL if not already done:

.. code-block:: sh

	sudo apt update
	sudo apt install postgresql postgresql-client
	sudo systemctl enable --now postgresql

Log to postgres:

.. code-block:: sh

	su - postgres
	psql

Create a database and a user for provider:

.. code-block:: sql

	CREATE DATABASE provider;
	CREATE USER <your_user> WITH PASSWORD '<your_password>';
	GRANT ALL PRIVILEGES ON DATABASE provider TO <your_user>;

Nginx configuration
************************

Now we need Nginx to serve our server to the public. If not already done, install Nginx:

.. code-block:: sh

	sudo apt update
	sudo apt install nginx
	# Certbot for HTTPS setup
	sudo apt install python3-acme python3-certbot python3-mock python3-openssl python3-pkg-resources python3-pyparsing python3-zope.interface
	sudo apt install python3-certbot-nginx

Create your Nginx server:

.. code-block:: sh

	sudo nano /etc/nginx/sites-available/<your_domain>

And write your configuration like this one for example:

.. code-block:: nginx

	server {
        index index.html index.htm index.nginx-debian.html;

        location / {
                proxy_pass http://127.0.0.1:8000;
                proxy_http_version 1.1;
                proxy_set_header Upgrade $http_upgrade;
                proxy_set_header Connection "upgrade";

                proxy_redirect off;
                proxy_set_header Host $host;
                proxy_set_header X-Real-IP $remote_addr;
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_set_header X-Forwarded-Host $server_name;
        }

        location /static/ {
                root <path_to_provider_repository>;
                add_header 'Access-Control-Allow-Origin' '*';
        }

        location /media/ {
                root <path_to_provider_repository>;
                add_header 'Access-Control-Allow-Origin' '*';
        }

        location /favicon.ico {
                root <path_to_provider_repository>/static/;
        }

        server_name <your_domain>;
	}

Restart Nginx:

.. code-block:: sh

	sudo service nginx restart

Setup HTTPS:

.. code-block:: sh

	sudo certbot --nginx -d <your_domain> -d <your_domain>

Production environment setup
******************************

Go in the root folder of the project. You will have to create a virtualenv with Python virtualenv

.. code-block:: sh

	python3 -m venv venv

And activate it

.. code-block:: sh

	source venv/bin/activate # Linux

Then, you can install the development dependencies with:

.. code-block:: sh

	pip3 install -r requirements/production.txt

Launching code in production
*****************************

Before restarting supervisor, you need to setup your environment variables in a ``.env`` at ``src`` like so:

.. code-block:: ini

	export TOKEN_PROD=<your_token>
	export DB_NAME=provider
	export DB_USER=<your_user>
	export DB_PASSWORD=<your_password>
	export DB_HOST=localhost
	export DB_PORT=5432
	export REDIS_PASSWORD=<your_redis_password>
	export BACKEND_TOKEN=<your_backend_token>
	export SENTRY_DSN=<your_sentry_dsn> # optional

.. Note::

	You can randomly generate  ``TOKEN_PROD``; it will be used by Django as seed. Also, you will need to access the admin panel before getting ``BACKEND_TOKEN``.

Now initialise your database:

.. code-block:: sh

	python3 manage.py makemigrations
	python3 manage.py migrate
	python3 manage.py createsuperuser # complete form to create an admin user

Restart supervisor with ``sudo service supervisor restart``
And access the admin panel at ``<your_domain>/admin`` using the created superuser. Create a BackendWorker and copy token to ``.env``. You can restart supervisor one more time and you're ready!
