**************************
Provider
**************************

.. image:: https://gitlab.com/bob_project/provider/badges/master/pipeline.svg
   :target: https://gitlab.com/bob_project/provider/-/commits/master
   :alt: Pipeline Status
.. image:: https://gitlab.com/bob_project/provider/badges/master/coverage.svg
   :target: https://bob_project.gitlab.io/provider/html/
.. image:: https://img.shields.io/badge/License-MIT-yellow.svg
   :target: https://opensource.org/licenses/MIT
   :alt: MIT license

Documentation : https://bob_project.gitlab.io/provider/
