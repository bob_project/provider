""" Provider interface app configuration """

from django.apps import AppConfig

class ProviderInterfaceConfig(AppConfig):
    """ Application configuration """

    name = 'provider_interface'
