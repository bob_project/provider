"""
This module describe administration panel configuration for models of scientist interface
"""

from django.contrib import admin
from django.utils.safestring import mark_safe
from django.utils.text import Truncator

from provider_interface.models import Team, Scientist, Project

@admin.register(Team)
class AdminTeam(admin.ModelAdmin):
    """ Admin configuration for Team model. """

    list_display = ('name',)
    list_filter = ('name',)
    ordering = ('name',)
    search_fields = ('name',)
    fields = ('name',)

@admin.register(Scientist)
class AdminScientist(admin.ModelAdmin):
    """ Admin configuration for Scientist model. """

    list_display = ('user', 'team')
    list_filter = ('user', 'team')
    ordering = ('user', 'team')
    search_fields = ('user', 'team')
    fields = ('user', 'team')

@admin.register(Project)
class AdminProject(admin.ModelAdmin):
    """ Admin configuration for Project model. """

    list_display = ('name', 'team', 'source_r', 'saboteur_source_r', 'wrapper', 'filled')
    list_filter = ('name', 'team', 'glue', 'wasm', 'glue_saboteur', 'wasm_saboteur')
    ordering = ('name', 'team', 'glue', 'wasm', 'glue_saboteur', 'wasm_saboteur')
    search_fields = ('name', 'team', 'glue', 'wasm', 'glue_saboteur', 'wasm_saboteur')
    fields = (
        'name', 'team', 'slug', 'description', 'source', 'saboteur_source',
        'glue', 'wasm', 'glue_saboteur', 'wasm_saboteur', 'wrapper'
    )
    readonly_fields = ('slug',)

    def source_r(self, obj): # pylint: disable=no-self-use
        """ Show a reduced ling on the interface to the source file. """

        html = '-'
        if obj.source.name:
            html = '<a href="/media/{}">{}</a>'.format(obj.source, Truncator(obj.source).chars(30, truncate='...'))
        return mark_safe(html)

    def saboteur_source_r(self, obj): # pylint: disable=no-self-use
        """ Show a reduced ling on the interface to the saboteur source file. """

        html = '-'
        if obj.saboteur_source.name:
            html = '<a href="/media/{}">{}</a>'.format(obj.saboteur_source, Truncator(obj.saboteur_source).chars(30, truncate='...'))
        return mark_safe(html)

    def filled(self, obj): # pylint: disable=no-self-use
        """ Tell if the project if corretly filled or not. """

        return obj.filled()
    filled.boolean = True
