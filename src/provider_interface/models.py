"""
Models description for scientist interface
"""

import logging
import os

from django.conf import settings
from django.db import models
from django.db.models.signals import post_delete, pre_save
from django.dispatch import receiver

from slugify import slugify

logger = logging.getLogger('interface')

def get_code_path(instance, filename):
    """ Return path to code file. """

    return "code/{}/{}".format(instance.slug, filename)

def get_saboteur_code_path(instance, filename):
    """ Return path to saboteur code file. """

    return "code/{}/saboteur/{}".format(instance.slug, filename)

class Team(models.Model):
    """ A scientist team """

    name = models.CharField(max_length=60)

    def __str__(self):
        return self.name

class Scientist(models.Model):
    """ An user profile for a scientist team member """

    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    team = models.ForeignKey(Team, on_delete=models.CASCADE)

class Project(models.Model):
    """
    A data structure to store .wasm and .js
    codes which will be distributed to workers
    """

    name = models.CharField(max_length=60, unique=True)
    slug = models.CharField(max_length=60, unique=True)
    description = models.TextField(max_length=1000, null=True, blank=True)
    team = models.ForeignKey(Team, on_delete=models.CASCADE)

    source = models.FileField(upload_to=get_code_path)
    saboteur_source = models.FileField(upload_to=get_saboteur_code_path)

    glue = models.FileField(null=True, blank=True, upload_to=get_code_path)
    wasm = models.FileField(null=True, blank=True, upload_to=get_code_path)
    glue_saboteur = models.FileField(null=True, blank=True, upload_to=get_saboteur_code_path)
    wasm_saboteur = models.FileField(null=True, blank=True, upload_to=get_saboteur_code_path)

    wrapper = models.CharField(max_length=150, null=True, blank=True)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        self.slug = slugify(self.name)

        if ((self.glue.name or self.wasm.name or self.glue_saboteur.name or self.wasm_saboteur.name) and not
            (self.glue.name or self.wasm.name)):
            raise Exception("You must provide 2 principle code files before saving this instance")
        super(Project, self).save(force_insert=force_insert, force_update=force_update, using=using, update_fields=update_fields) # pylint: disable=super-with-arguments

    def filled(self):
        """ Tell if the project is correctly filled. """

        return all([self.wasm.name, self.glue.name, self.wasm_saboteur.name, self.glue_saboteur.name])

    def __str__(self):
        return self.name

@receiver(post_delete, sender=Project)
def auto_delete_project_file_on_delete(sender, instance, **kwargs): # pylint: disable=unused-argument,too-many-branches
    """ Deletes file from filesystem when corresponding `Project` object is deleted. """

    if instance.glue:
        if os.path.isfile(instance.glue.path):
            logger.debug("Delete old file : {}".format(instance.glue.path))
            try:
                os.remove(instance.glue.path)
            except:
                pass
    if instance.wasm:
        if os.path.isfile(instance.wasm.path):
            logger.debug("Delete old file : {}".format(instance.wasm.path))
            try:
                os.remove(instance.wasm.path)
            except:
                pass
    if instance.glue_saboteur:
        if os.path.isfile(instance.glue_saboteur.path):
            logger.debug("Delete old file : {}".format(instance.glue_saboteur.path))
            try:
                os.remove(instance.glue_saboteur.path)
            except:
                pass
    if instance.wasm_saboteur:
        if os.path.isfile(instance.wasm_saboteur.path):
            logger.debug("Delete old file : {}".format(instance.wasm_saboteur.path))
            try:
                os.remove(instance.wasm_saboteur.path)
            except:
                pass
    if instance.source:
        if os.path.isfile(instance.source.path):
            logger.debug("Delete old file : {}".format(instance.source.path))
            try:
                os.remove(instance.source.path)
            except:
                pass
    if instance.saboteur_source:
        if os.path.isfile(instance.saboteur_source.path):
            logger.debug("Delete old file : {}".format(instance.saboteur_source.path))
            try:
                os.remove(instance.saboteur_source.path)
            except:
                pass

@receiver(pre_save, sender=Project)
def auto_delete_project_file_on_change(sender, instance, **kwargs): # pylint: disable=unused-argument,too-many-branches
    """ Deletes old file from filesystem when corresponding `Project` object is updated with new file. """

    if not instance.pk:
        return

    try:
        old = Project.objects.get(pk=instance.pk)
    except Project.DoesNotExist:
        return

    if old.glue.name and not old.glue == instance.glue:
        if os.path.isfile(old.glue.path):
            logger.debug("Delete old file : {}".format(instance.glue.path))
            try:
                os.remove(old.glue.path)
            except:
                pass
    if old.wasm.name and not old.wasm == instance.wasm:
        if os.path.isfile(old.wasm.path):
            logger.debug("Delete old file : {}".format(instance.wasm.path))
            try:
                os.remove(old.wasm.path)
            except:
                pass
    if old.glue_saboteur.name and not old.glue_saboteur == instance.glue_saboteur:
        if os.path.isfile(old.glue_saboteur.path):
            logger.debug("Delete old file : {}".format(instance.glue_saboteur.path))
            try:
                os.remove(old.glue_saboteur.path)
            except:
                pass
    if old.wasm_saboteur.name and not old.wasm_saboteur == instance.wasm_saboteur:
        if os.path.isfile(old.wasm_saboteur.path):
            logger.debug("Delete old file : {}".format(instance.wasm_saboteur.path))
            try:
                os.remove(old.wasm_saboteur.path)
            except:
                pass
    if old.source.name and not old.source == instance.source:
        if os.path.isfile(old.source.path):
            logger.debug("Delete old file : {}".format(instance.source.path))
            try:
                os.remove(old.source.path)
            except:
                pass
    if old.saboteur_source.name and not old.saboteur_source == instance.saboteur_source:
        if os.path.isfile(old.saboteur_source.path):
            logger.debug("Delete old file : {}".format(instance.saboteur_source.path))
            try:
                os.remove(old.saboteur_source.path)
            except:
                pass
