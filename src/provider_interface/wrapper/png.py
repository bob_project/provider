"""
A Wrapper for PNG tasks
"""

import os
import logging
import requests
from PIL import Image

from django.conf import settings
from django.utils.safestring import mark_safe

from provider_backend.models import Variable

from provider_interface.wrapper import Wrapper

logger = logging.getLogger('backend')

class PNGWrapper(Wrapper):
    """ A Wrapper for PNG tasks """

    def generate_dataset(self, output_dir=settings.DEFAULT_DATASET_FOLDER):

        number = int(Variable.objects.get(name="DEFAULT_PNG_NUMBER").value)
        size = int(Variable.objects.get(name="DEFAULT_PNG_SIZE").value)

        logger.debug("Downloading {number} images with the following dimensions: {size} * {size} in directory : {output_dir}".format(
            number=number, size=size, output_dir=output_dir
        ))
        url = "https://picsum.photos/{size}/{size}".format(size=size)

        for index in range(number):
            img_data = requests.get(url).content
            filename = "{}{}.jpg".format(output_dir, index)
            logger.debug("Downloaded image : {}".format(filename))
            with open(filename, 'wb') as file:
                file.write(img_data)

            # convert JPG to PNG
            image = Image.open(filename)
            new_filename = filename.replace('jpg', 'png')
            image.save(new_filename)
            logger.debug("Converted image to png : {}".format(new_filename))
            os.remove(filename)

    def preview(self, file):
        return mark_safe('<img src="/media/{}" width="50" loading="lazy"/>'.format(file))
    