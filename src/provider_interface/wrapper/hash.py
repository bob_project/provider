"""
a "Value" is an array representing a number in any base
the Value 243 in base 10 is [2,4,3]
the Value 12 in base 2 is [1,1,0,0]
"""

import hashlib
import logging

from struct import pack

from django.conf import settings
from django.utils.text import Truncator

from provider_backend.models import Variable

from provider_interface.wrapper import Wrapper

logger = logging.getLogger('backend')

class HashWrapper(Wrapper):
    """ A Wrapper for HASH tasks """

    def _write_blob(self, index, md5, tries, size, string, output_dir="."): # pylint: disable=no-self-use,too-many-arguments
        """ Write a task data to output_dir """

        filename = "{}/bf_{}_{}.raw".format(output_dir, str(size), str(index))
        with open(filename, "wb") as file:
            file.write(md5)
            file.write(pack("i", tries))
            file.write(pack("i", size))
            file.write(string.encode())
            file.write(b"\x00")
        logger.debug("Created new blob at : {}".format(filename))

    def _int_to_value(self, value, base): # pylint: disable=no-self-use
        """ Convert a decimal number in a Value. """

        result = []
        while value != 0:
            digit = value % base
            value = value // base
            result.append(digit)

        return result[::-1]

    def _string_to_value(self, string, alphabet): # pylint: disable=no-self-use
        """ Convert a string to a Value, the base is the alphabet length. """

        result = []
        for char in string:
            result.append(alphabet.index(char))
        return result

    def _value_to_string(self, value, alphabet): # pylint: disable=no-self-use
        """ Convert a Value to string using the given alphabet. """

        result = ""
        for char in value:
            result += alphabet[char]
        return result

    def _adjust_size(self, tab_1, tab_2): # pylint: disable=no-self-use
        """ Make tab_1 the same size as tab_2. """

        while len(tab_1) < len(tab_2):
            tab_1 = [0] + tab_1

        return tab_1

    def _addition(self, val_1, val_2, base):
        """ Add two Values. """

        # make the two Values the same size
        if len(val_1) < len(val_2):
            val_1 = self._adjust_size(val_1, val_2)
        elif len(val_1) > len(val_2):
            val_2 = self._adjust_size(val_2, val_1)

        # swapping endianness make going through array easier
        val_1 = val_1[::-1]
        val_2 = val_2[::-1]

        result = []
        for i in range(len(val_1)): # pylint: disable=consider-using-enumerate

            full_sum = val_1[i] + val_2[i]
            digit = full_sum % base
            carry = full_sum // base
            result.append(digit)

            if carry != 0:
                if i == len(val_1)-1:
                    result.append(carry)
                else:
                    val_1[i+1] += carry

        result = result[::-1]
        return result

    def _string_add(self, string, number, alphabet):
        """ Increment a string by a number using 'Values' """

        base = len(alphabet)
        result = ""
        string = self._string_to_value(string, alphabet)
        value = self._int_to_value(number, base)

        result = self._addition(string, value, base)
        result = self._value_to_string(result, alphabet)

        return result

    def generate_dataset(self, output_dir=settings.DEFAULT_DATASET_FOLDER):

        min_size = int(Variable.objects.get(name="DEFAULT_HASH_WORD_MIN_SIZE").value)
        max_size = int(Variable.objects.get(name="DEFAULT_HASH_WORD_MAX_SIZE").value)
        max_attempts = pow(10, int(Variable.objects.get(name="DEFAULT_HASH_ATTEMPTS").value))
        md5_hash = hashlib.md5(bytes(Variable.objects.get(name="DEFAULT_TO_FOUND").value, encoding="utf-8")).digest()

        alphabet = "!\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~"
        alphabet_size = len(alphabet)

        for index in range(min_size, max_size + 1):

            count = 0
            string = "".join([alphabet[0] for _ in range(index)])
            i_attempts = pow(alphabet_size, index)

            # in total, there is a * max_attempts + b strings
            var_a = i_attempts // max_attempts
            var_b = i_attempts % max_attempts - 1

            count += 1
            for _ in range(var_a):
                # write blobs with max_attempts
                self._write_blob(count, md5_hash, max_attempts, index, string, output_dir)
                string = self._string_add(string, max_attempts, alphabet)
                count += 1

            # write last blob with b attempts
            self._write_blob(count, md5_hash, var_b, index, string, output_dir)

    def preview(self, file): # pylint: disable=no-self-use
        html = '-'
        data = file.read()
        # string not found in this task
        if data[0] == 0x00:
            html = '-'
        # string found in this task
        elif data[0] == 0x01:

            # remove trailing zeros
            data = list(filter(None,data))

            string = [chr(i) for i in data[1:]]
            string = "".join(string)

            html = Truncator(string).chars(20, truncate='...')
        else:
            html = 'ERROR'
        return html
