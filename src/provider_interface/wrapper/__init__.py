""" A generic wrapper definition for a project """

from abc import ABC, abstractmethod

from django.conf import settings

class Wrapper(ABC):
    """ A generic wrapper for a project """

    @abstractmethod
    def generate_dataset(self, output_dir=settings.DEFAULT_DATASET_FOLDER):
        """ Generate a dataset for this type of project """

    @abstractmethod
    def preview(self, file):
        """ Return the preview for the project data """
