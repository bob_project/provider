"""
A WebSocket Client running worker scheduler wich will attributes tasks to workers,
send them work notifications and aggregate their results
"""

import asyncio
import datetime
import logging
import os

from django.conf import settings
from django.core.management.base import BaseCommand
from django.utils import timezone

from channels.db import database_sync_to_async
from dotenv import load_dotenv
from tqdm import tqdm

from provider.sentry import init

from provider_backend.models import Job, Worker, Variable
from provider_backend.utils.creation import init_backend, create_default_job
from provider_backend.utils.input import get_mode
from provider_backend.utils.utils import WebhookManager
from provider_backend.websocket_client import WebSocketClient, StopException

from provider_coordinator.models import Computer, Batch, BatchWorker
from provider_coordinator.distributor import Assign
from provider_coordinator.utils import create_excel

from provider_interface.models import Project

logger = logging.getLogger('coordinator')

init()
load_dotenv(verbose=True, override=True, dotenv_path='.env')

class TimeoutException(Exception):
    """ A simple utility exception for Batch timeout. """

    def __init__(self, message):
        super().__init__()
        self.message = message

class BatchManager: # pylint: disable=too-many-instance-attributes
    """
    A wrapper function to handle all database calls for CoordinatorClient

    Attributes:
        webhook:       WebhookManager
        configuration: Run configuration
    """

    def __init__(self, url, name, silent, configuration):

        self.webhook = WebhookManager(url, name, silent)
        self.configuration = configuration
        self.total = 0
        self.set_total()
        self.count = 0
        self.total_run = 0
        self.count_run = 0
        self.batch_timeout = int(Variable.objects.get(name="BATCH_TIMEOUT").value)

        self.job = None
        self.batch = None

    def set_total(self): # pylint: disable=too-many-nested-blocks
        """ Calculate total number of batch """

        self.total = 0
        for _ in self.configuration['projects']:
            for mode_attribute in self.configuration['mode_attributes']:
                for mode_vote in self.configuration['mode_votes']:
                    for _ in self.configuration['shadow']:
                        for _ in self.configuration['saboteur_rates']:
                            if mode_vote == 'CREDIB' and mode_attribute == 'SPOTCK' or (mode_vote != 'CREDIB' and mode_attribute != 'SPOTCK'):
                                self.total += 1

    @database_sync_to_async
    def create_job(self, project):
        """  Create a defaut job with provided task type and clear old data. """

        Job.objects.all().delete()
        Worker.objects.all().filter(token='').delete()
        self.job = create_default_job(project)

    @database_sync_to_async
    def create_batch(self, project, mode_attribute, mode_vote, saboteur_rate, shadow): # pylint: disable=too-many-arguments
        """ Create a batch with provided configuration. """

        self.total_run = 0
        self.count_run = 0
        # reset manager job for this new batch
        self.job.reset()
        self.job.mode_vote = mode_vote
        self.job.mode_attribute = mode_attribute
        self.job.shadow = shadow
        self.job.save()

        global_configuration = None
        if self.job.check_modes():

            computers = Computer.objects.filter(available=True)
            configurations = Assign(computers, saboteur_rate).assign_all()
            computers.update(state='WAITING', finished=False)

            self.batch = Batch(
                project=project,
                mode_attribute=mode_attribute,
                mode_vote=mode_vote,
                saboteur_rate=saboteur_rate,

                threshold=self.job.threshold,
                replications=self.job.replications,
                spot_check_prob=self.job.spot_check_prob,
                shadow=shadow,

                nb_short=configurations["nb_short"],
                nb_long=configurations["nb_long"],
                nb_saboteur=configurations["nb_saboteur"],
            )
            self.batch.save()

            self.count += 1
            logger.debug("START [{}/{}]\t{}\t{}\t{}\t{}  \t{}".format(
                self.count, self.total,
                self.batch.project,
                'SHADOW' if self.batch.shadow else 'CLASSIC',
                self.batch.mode_attribute,
                self.batch.mode_vote,
                self.batch.saboteur_rate,
            ))
            self.webhook.status(self.batch, state="START", title="[{}/{}]".format(self.count, self.total))
            global_configuration = configurations["configurations"]

        return global_configuration

    @database_sync_to_async
    def all_finish(self): # pylint: disable=no-self-use
        """ Check if all computer finished batch. """

        computers = Computer.objects.filter(available=True)
        check = computers.filter(finished=True).count() == computers.count()
        if check:
            # if all finished, change there state to UP and finished to False
            computers.update(state='UP', finished=False)
        return check

    @database_sync_to_async
    def save_stats(self):
        """ Save job stats on current batch and all Workers as BatchWorker. """

        logger.debug("SAVE  [{}/{}]\t{}\t{}\t{}\t{}  \t{}".format(
            self.count, self.total,
            self.batch.project,
            'SHADOW' if self.batch.shadow else 'CLASSIC',
            self.batch.mode_attribute,
            self.batch.mode_vote,
            self.batch.saboteur_rate,
        ))
        self.job.refresh_from_db()
        stats = self.job.stats()
        self.batch.completion = stats["completion"]

        # duration stats
        self.batch.job_duration = stats["job_duration"].total_seconds()
        self.batch.task_duration = stats["task_duration"].total_seconds()
        self.batch.computation_duration = stats["computation_duration"].total_seconds()
        self.batch.vote_duration = stats["vote_duration"].total_seconds()
        self.batch.attribute_duration = stats["attribute_duration"].total_seconds()

        # integeer stats
        self.batch.network_load = stats["global_load"]
        self.batch.task_replications = stats["task_replications"]
        self.batch.shadow_count = stats["shadow_count"]
        self.batch.rerolls = stats["rerolls"]
        self.batch.save()

        logger.debug("Saving Batch Workers")
        # save each worker as batchworker
        for worker in Worker.objects.filter(token=""):
            BatchWorker(
                batch=self.batch,
                state=worker.state,
                last_seen=worker.last_seen,
                banned=worker.banned,
                saboteur=worker.saboteur,
                backtrack_cost=worker.backtrack_cost,
                run_duration=worker.run_duration,
                creation=worker.creation,
                ban_date=worker.ban_date,
            ).save()
        # notify discord webhook
        self.webhook.status(self.batch, state="END", title="[{}/{}]".format(self.count, self.total))
        logger.debug("End batch : {}".format(self.batch))

    @database_sync_to_async
    def wait_spot_check_creation(self):
        """ Check if job has enough spot check tasks """

        return self.job.need_spot_check_tasks()

    @database_sync_to_async
    def generate_excel(self):
        """ Generate excel file with all batch """

        batchs = Batch.objects.all()
        path = "statistics.xlsx"
        create_excel(batchs, path)
        self.webhook.result(path)

class CoordinatorClient(WebSocketClient): # pylint: disable=too-many-instance-attributes
    """
    This client handle websocket connection and interaction with Scheduler

    Attributes:
        manager: BatchManager
    """

    def __init__(self, backend, webhook_url, silent, configuration):

        super().__init__(backend)
        self.manager = BatchManager(webhook_url, "Coordinator", silent, configuration)
        self.add_task(self.master_task(self.coordinate, repeat=False))

    async def start(self, configuration, batch):
        """ Send a notification to computer to start a new run. """

        # create json message to send
        message = {
            'type': 'START_CLIENT',
            'nb_short': configuration['nb_short'],
            'nb_long': configuration['nb_long'],
            'nb_saboteur': configuration['nb_saboteur'],
            'room': configuration['computer'].room,
            'batch': str(batch),
        }
        await self.send(message)

    @database_sync_to_async
    def receive(self, message): # pylint: disable=invalid-overridden-method
        """ Receive all messages from websocket connection. """

        if 'computer' in message:
            computer = Computer.objects.get(room=message['computer'])
            if message['type'] == 'START_RUN':
                computer.state = 'RUNNING'
                self.manager.total_run += message['total_run']
            elif message['type'] == 'FINISHED_RUN':
                computer.finished = True
            elif message['type'] == 'END_RUN':
                self.manager.count_run += 1
            computer.save()
        else:
            logger.warning("Message doesn't contain computer reference : {}".format(message))

    async def coordinate(self):
        """
        Main event loop which coordinate all batches
        """

        try:
            # loop on each data type
            for project in self.manager.configuration['projects']:
                # create dataset for all batchs of this type
                await self.manager.create_job(project)
                # wait spot check task creation
                while await self.manager.wait_spot_check_creation():
                    await asyncio.sleep(self.backend.delay)
                # loop on all variables
                for mode_attribute in self.manager.configuration['mode_attributes']:
                    for mode_vote in self.manager.configuration['mode_votes']:
                        for shadow in self.manager.configuration['shadow']:
                            for saboteur_rate in self.manager.configuration['saboteur_rates']:

                                distributions = await self.manager.create_batch(project, mode_attribute, mode_vote, saboteur_rate, shadow)
                                if distributions:

                                    # send orders to computers
                                    for distribution in distributions:
                                        await self.start(distribution, self.manager.batch)

                                    # wait all computer to finish
                                    start = timezone.now()
                                    header = "[{}][{}/{}] ".format(start.strftime("%d/%m/%Y %H:%M:%S"), self.manager.count, self.manager.total)
                                    footer = " {} {} {} {} {}".format(
                                        self.manager.batch.project,
                                        'SHADOW' if self.manager.batch.shadow else 'CLASSIC',
                                        self.manager.batch.mode_attribute,
                                        self.manager.batch.mode_vote,
                                        self.manager.batch.saboteur_rate,
                                    )

                                    bar_format = header + '{percentage:3.0f}%|{bar:30}{r_bar}' + footer
                                    with tqdm(total=self.manager.total_run, bar_format=bar_format) as pbar:
                                        while not await self.manager.all_finish():
                                            await asyncio.sleep(self.backend.delay)
                                            if await self.check_stop():
                                                raise StopException()

                                            delta = timezone.now() - start
                                            delta = delta - datetime.timedelta(microseconds=delta.microseconds)

                                            pbar.total = self.manager.total_run
                                            pbar.n = self.manager.count_run
                                            pbar.refresh()

                                            # if reach maximum timeout, raise exception
                                            if delta.total_seconds() / 60 > self.manager.batch_timeout:
                                                raise TimeoutException("Batch started since more than {} minutes".format(self.manager.batch_timeout))

                                    await self.manager.save_stats()
            await self.manager.generate_excel()
        except TimeoutException as exception:
            logger.error(exception.message)
            # notify discord webhook
            self.manager.webhook.error(exception.message)

        logger.info("End of coordination")

class Command(BaseCommand):
    """ A custom Django command to launch coordinator """

    help = 'Start coordinator backend'

    def add_arguments(self, parser):

        parser.add_argument('--stop', action='store_true', help='Stop backend worker')
        parser.add_argument('--silent', action='store_true', help="Don't send discord notification")
        parser.add_argument('--clean', action='store_true', help="Delete old batchs")

    def get_variable_field(self, message, default): # pylint: disable=no-self-use
        """ Get all fields for a specific variable """

        field = default
        print("\033[30;1mPick variable field for : \033[0m\033[33m{}\033[0m".format(message))
        if not input("\033[30;1mKeep default ? \033[0m\033[33m{} [y/n] \033[0m".format(default)).startswith('y'):
            field = []
            for value in default:
                if input("\033[30;1mUse \033[0m\033[33m{}\033[30;1m ? \033[0m\033[33m[y/n] \033[0m".format(value)).startswith('y'):
                    field.append(value)
            if not field:
                field.append(default[0])
        print()
        return field

    def get_projects(self): # pylint: disable=no-self-use
        """ Get chosen projects from user """

        projects = Project.objects.all()
        print("\033[30;1mPick variable field for : \033[0m\033[33m{}\033[0m".format("PROJECTS"))
        if not input("\033[30;1mKeep default ? \033[0m\033[33m{} [y/n] \033[0m".format(list(projects))).startswith('y'):
            field = []
            for project in projects:
                if input("\033[30;1mUse \033[0m\033[33m{}\033[30;1m ? \033[0m\033[33m[y/n] \033[0m".format(project.name)).startswith('y'):
                    field.append(project)
            if not field:
                field.append(projects.first())
        else:
            field = list(projects)
        print()
        return field

    def get_configuration(self):
        """ Retrieve run configuration """

        saboteur_rates = [float(entry) for entry in Variable.objects.get(name="SABOTEUR_RATES").value.split(',')]

        return {
            "projects": self.get_projects(),
            "mode_attributes": self.get_variable_field("ATTRIBUTORS", get_mode(settings.ATTRIBUTORS)),
            "mode_votes": self.get_variable_field("VOTERS", get_mode(settings.VOTERS)),
            "saboteur_rates": self.get_variable_field("SABOTEUR_RATES", saboteur_rates),
            "shadow": self.get_variable_field("SHADOW", [True, False]),
        }

    def handle(self, *args, **options):
        """ Entry point of the command """

        try:
            # retrieve context variables
            token = os.environ.get("COORDINATOR_TOKEN")
            webhook_url = os.environ.get("WEBHOOK_URL")

            if options["clean"]:
                logger.info("Clean old batchs")
                Batch.objects.all().delete()
                BatchWorker.objects.all().delete()

            backend = init_backend(token, options)
            if not options['stop'] and backend:
                configuration = self.get_configuration()
                CoordinatorClient(backend, webhook_url, options['silent'], configuration).run()
        except KeyboardInterrupt:
            logger.warning("Command interrupdated by keyboard")
