"""
Models for coordinator
"""

import logging

from django.conf import settings
from django.db import models
from django.utils import timezone

from provider_backend.models import get_token, get_mode, Worker
from provider_backend.utils.utils import print_delta

from provider_interface.models import Project

logger = logging.getLogger('coordinator')

class Computer(models.Model):
    """ A computer of the project team """

    COMPUTER_STATE = (
        ('UP', 'UP'),
        ('WAITING', 'WAITING'),
        ('RUNNING', 'RUNNING'),
        ('DOWN', 'DOWN'),
    )

    name = models.CharField(max_length=50)
    token = models.CharField(max_length=50, default=get_token, unique=True)
    room = models.CharField(max_length=50, default=get_token, unique=True)
    state = models.CharField(max_length=10, choices=COMPUTER_STATE, default=COMPUTER_STATE[0][0])
    thread_nb = models.IntegerField(default=0)
    margin = models.IntegerField(default=1)
    available = models.BooleanField(default=False)
    finished = models.BooleanField(default=False)

    def __str__(self):
        return "{}_{} ({})".format(self.name, self.thread_nb, self.state)

class BatchWorker(models.Model):
    """ A data structure to save worker metrics """

    batch = models.ForeignKey('Batch', on_delete=models.CASCADE)
    state = models.CharField(max_length=10, choices=Worker.WORKER_STATE, default=Worker.WORKER_STATE[1][0])
    last_seen = models.DateTimeField(default=timezone.now)
    banned = models.BooleanField(default=False)
    saboteur = models.BooleanField(default=False)
    backtrack_cost = models.IntegerField(default=0)
    run_duration = models.FloatField(default=0.0)
    creation = models.DateTimeField(default=timezone.now)
    ban_date = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return str(self.id)

class Batch(models.Model):
    """ A data structure to save job metrics """

    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    saboteur_rate = models.FloatField(default=0.0)
    mode_attribute = models.CharField(max_length=50, choices=get_mode(settings.ATTRIBUTORS), default=get_mode(settings.ATTRIBUTORS)[0][0])
    mode_vote = models.CharField(max_length=50, choices=get_mode(settings.VOTERS), default=get_mode(settings.VOTERS)[0][0])

    shadow = models.BooleanField(default=False)
    threshold = models.IntegerField(default=5)
    replications = models.IntegerField(default=5)
    spot_check_prob = models.FloatField(default=0.25)

    completion = models.FloatField(default=0.0)
    network_load = models.IntegerField(default=0)
    task_replications = models.IntegerField(default=0)
    rerolls = models.IntegerField(default=0)
    shadow_count = models.IntegerField(default=0)

    job_duration = models.FloatField(default=0.0)
    task_duration = models.FloatField(default=0.0)
    computation_duration = models.FloatField(default=0.0)
    vote_duration = models.FloatField(default=0.0)
    attribute_duration = models.FloatField(default=0.0)

    nb_short = models.IntegerField(default=-1)
    nb_long = models.IntegerField(default=-1)
    nb_saboteur = models.IntegerField(default=-1)

    def __str__(self):
        return "<Batch: type={}, saboteur_rate={}, attribute={}, vote={}>".format(
            self.project, self.saboteur_rate, self.mode_attribute, self.mode_vote
        )

    def stats(self):
        """ Return Batch statistics as JSON """

        workers = BatchWorker.objects.filter(batch=self)

        return {
            "project": self.project.slug,
            "mode_attribute": self.mode_attribute,
            "mode_vote": self.mode_vote,
            "saboteur_rate": self.saboteur_rate,

            "shadow": self.shadow,
            "threshold": self.threshold,
            "replications": self.replications,
            "spot_check_prob": self.spot_check_prob,

            "completion": self.completion,
            "global_load": self.network_load,
            "task_replications": self.task_replications,
            "rerolls": self.rerolls,
            "shadow_count": self.shadow_count,

            "job_duration": print_delta(self.job_duration),
            "task_duration": print_delta(self.task_duration),
            "computation_duration": print_delta(self.computation_duration),
            "vote_duration": print_delta(self.vote_duration),
            "attribute_duration": print_delta(self.attribute_duration),

            "true_positive": workers.filter(saboteur=True, banned=True).count(),
            "true_negative": workers.filter(saboteur=False, banned=False).count(),
            "false_positive": workers.filter(saboteur=False, banned=True).count(),
            "false_negative": workers.filter(saboteur=True, banned=False).count(),

            "nb_short": self.nb_short,
            "nb_long": self.nb_long,
            "nb_saboteur": self.nb_saboteur,
        }
