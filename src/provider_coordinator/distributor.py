"""
Utility module to calculate ditribution of workers between long, short and saboteur
"""

import logging

from provider_backend.models import Variable

logger = logging.getLogger('coordinator')

class Assign: # pylint: disable=too-few-public-methods
    """
    A utility finction to calculate worker repartion between available computers.

    Parameters:
        computers: List of available computers
        rate_saboteur: Rate of saboteurs in the system
        rate_short: Rate of short worker in fair workers
    """

    def __init__(self, computers, rate_saboteur, rate_short=None):

        if not rate_short:
            rate_short = float(Variable.objects.get(name="RATE_SHORT_WORKER").value)

        self.computers = computers
        self.threads_nb = self._get_threads_nb()
        self.rate_long = 1 - rate_short
        self._set_numbers(rate_saboteur)

    def _set_numbers(self, rate_saboteur):
        """
        Caclulate number of worker of each type (short, long, saboteur).

        Parameters:
            rate_saboteur: Rate of saboteurs in the system
        """

        logger.debug("Available threads: {}".format(self.threads_nb))

        # calculate number of saboteur
        self.nb_saboteur = round(self.threads_nb * rate_saboteur)
        remaining = self.threads_nb - self.nb_saboteur
        # calculate number of long worker
        self.nb_long = round(remaining * self.rate_long)
        # calculate number of short worker
        self.nb_short = self.threads_nb - self.nb_saboteur - self.nb_long

    def _get_threads_nb(self):
        """ Calculate total number of threads available. """

        total = 0
        for computer in self.computers:
            total += computer.thread_nb - computer.margin
        return total

    def _assign_numbers(self, count):
        """
        Distribute number of workers between computers.

        Parameters:
            count: Number of worker to distribute
        """

        assignation_dict = {}
        index = 0
        while index < count:
            for computer in self.computers:
                if computer.available_cores > 0 and index < count:

                    # add a worker to computer
                    if computer.id not in assignation_dict:
                        assignation_dict[computer.id] = 1
                    else:
                        assignation_dict[computer.id] += 1
                    index += 1
                    computer.available_cores -= 1
                # break when reach number of worker to distribute
                elif index >= count:
                    break

        return assignation_dict

    def assign_all(self):
        """ Distribute all workers of each type between computers. """

        logger.debug("Generating repartion configurations")
        # reseting the number of available cores each time we assign everything
        for computer in self.computers:
            computer.available_cores = computer.thread_nb - computer.margin

        nb_saboteur = self._assign_numbers(self.nb_saboteur)
        nb_long = self._assign_numbers(self.nb_long)
        nb_short = self._assign_numbers(self.nb_short)

        configurations = []
        for computer in self.computers:
            logger.debug("Generating repartion configuration for {}".format(computer))

            configuration = {
                'computer': computer,
                'nb_short': 0,
                'nb_long': 0,
                'nb_saboteur': 0,
            }

            # retrieve number of worker assigned to comptuer for each type of worker
            if computer.id in nb_saboteur:
                configuration['nb_saboteur'] = nb_saboteur[computer.id]
            if computer.id in nb_long:
                configuration['nb_long'] = nb_long[computer.id]
            if computer.id in nb_short:
                configuration['nb_short'] = nb_short[computer.id]

            logger.debug("Generated configuration : {}".format(configuration))
            configurations.append(configuration)

        return {
            "nb_short": self.nb_short,
            "nb_long": self.nb_long,
            "nb_saboteur": self.nb_saboteur,
            "configurations": configurations,
        }
