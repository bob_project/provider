"""
This module describe administration panel configuration for models of coordinator
"""

import logging

from os import remove

from django.contrib import admin
from django.http import HttpResponse
from django.utils.safestring import mark_safe

from provider_backend.utils.utils import print_delta
from provider_coordinator.models import Computer, Batch, BatchWorker
from provider_coordinator.utils import create_excel

logger = logging.getLogger('coordinator')

def generate_excel(modeladmin, request, queryset): # pylint: disable=unused-argument
    """ Generate a stats excel for Batchs. """

    path = "stats.xlsx"
    create_excel(queryset, path)

    # retrieve generated document
    bin_doc = open(path, 'rb')
    bin_doc_content = bin_doc.read()
    bin_doc.seek(0, 2)
    bin_doc_size = bin_doc.tell()
    bin_doc.close()

    # delete temporary document
    remove(path)
    # return document in HTTP response
    response = HttpResponse(
        bin_doc_content,
        content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    )
    response['Content-Length'] = bin_doc_size
    response['Content-Disposition'] = 'attachment; filename="{}"'.format(path)
    return response

generate_excel.short_description = "Generate Excel"

@admin.register(Computer)
class AdminComputer(admin.ModelAdmin):
    """ Admin configuration for Computer model. """

    list_display = ('name', 'token', 'room', 'state', 'finished', 'thread_nb', 'margin', 'available')
    list_filter = ('name', 'state', 'thread_nb', 'available', 'margin')
    ordering = ('name', 'thread_nb', 'state', 'thread_nb')
    search_fields = ('name', 'state', 'thread_nb')
    fields = ('name', 'token', 'room', 'state', 'thread_nb', 'margin', 'available', 'finished')

@admin.register(Batch)
class AdminBatch(admin.ModelAdmin):
    """ Admin configuration for Batch model. """

    list_display = (
        'id', 'configuration', 'stats'
    )
    list_filter = ('project', 'saboteur_rate', 'mode_attribute', 'mode_vote', 'shadow')
    ordering = ('project', 'saboteur_rate', 'mode_attribute', 'mode_vote', 'shadow')
    search_fields = ('project', 'saboteur_rate', 'mode_attribute', 'mode_vote', 'shadow')
    fields = (
        'project', 'saboteur_rate', 'mode_attribute', 'mode_vote', 'shadow',
        'completion', 'network_load', 'task_replications', 'rerolls',
        'shadow_count', 'job_duration', 'task_duration',
        'computation_duration', 'vote_duration', 'attribute_duration',
        'nb_short', 'nb_long', 'nb_saboteur',
    )
    actions = (generate_excel,)

    def configuration(self, obj): # pylint: disable=no-self-use
        """ Display Batch configuration """

        html_config = (
            "<table><tbody>"
            "<tr><td>Project</td><td>{}</td></tr>"
            "<tr><td>Saboteur rate</td><td>{}</td></tr>"
            "<tr><td>Mode attribute</td><td>{}</td></tr>"
            "<tr><td>Mode vote</td><td>{}</td></tr>"
            "<tr><td>Replications</td><td>{}</td></tr>"
            "<tr><td>Threshold</td><td>{}</td></tr>"
            "<tr><td>Spot Check Probability</td><td>{}</td></tr>"
            "<tr><td>Shadow</td><td>{}</td></tr>"
            "<tr><td>Short number</td><td>{}</td></tr>"
            "<tr><td>Long number</td><td>{}</td></tr>"
            "<tr><td>Saboteur number</td><td>{}</td></tr>"
            "</tbody></table>"
        ).format(
            obj.project,
            obj.saboteur_rate,
            obj.mode_attribute,
            obj.mode_vote,
            obj.replications,
            obj.threshold,
            obj.spot_check_prob,
            '<img src="/static/admin/img/icon-yes.svg" alt="True">' if obj.shadow else '<img src="/static/admin/img/icon-no.svg" alt="False">',
            obj.nb_short,
            obj.nb_long,
            obj.nb_saboteur,
        )
        return mark_safe(html_config)

    def stats(self, obj): # pylint: disable=no-self-use
        """ Display Batch stats """

        html_stats = (
            "<table><tbody>"
            "<tr><td>Completion</td><td>{} %</td></tr>"
            "<tr><td>Job duration</td><td>{}</td></tr>"
            "<tr><td>Task duration</td><td>{}</td></tr>"
            "<tr><td>Computation duration</td><td>{}</td></tr>"
            "<tr><td>Vote duration</td><td>{}</td></tr>"
            "<tr><td>Attribute duration</td><td>{}</td></tr>"
            "<tr><td>Network load</td><td>{} octets</td></tr>"
            "<tr><td>Task Replications</td><td>{}</td></tr>"
            "<tr><td>Shadow count</td><td>{}</td></tr>"
            "<tr><td>Rerolls</td><td>{}</td></tr>"
            "</tbody></table>"
        ).format(
            obj.completion,
            print_delta(obj.job_duration) if obj.job_duration else "-",
            print_delta(obj.task_duration) if obj.task_duration else "-",
            print_delta(obj.computation_duration) if obj.computation_duration else "-",
            print_delta(obj.vote_duration) if obj.vote_duration else "-",
            print_delta(obj.attribute_duration) if obj.attribute_duration else "-",
            obj.network_load,
            obj.task_replications,
            obj.shadow_count,
            obj.rerolls,
        )
        return mark_safe(html_stats)

@admin.register(BatchWorker)
class AdminBatchWorker(admin.ModelAdmin):
    """ Admin configuration for BatchWorker model. """

    list_display = ('batch', 'state', 'last_seen', 'banned', 'saboteur', 'backtrack_cost', 'display_duration', 'ban_delay')
    list_filter = ('batch', 'state', 'last_seen', 'banned', 'saboteur', 'backtrack_cost')
    ordering = ('batch', 'state', 'last_seen', 'banned', 'saboteur', 'backtrack_cost', 'run_duration', 'creation', 'ban_date')
    search_fields = ('batch', 'state', 'last_seen', 'banned', 'saboteur', 'backtrack_cost')
    fields = ('batch', 'state', 'last_seen', 'banned', 'saboteur', 'backtrack_cost', 'run_duration', 'creation', 'ban_date')

    def display_duration(self, obj): # pylint: disable=no-self-use
        """ Display run_duration """

        return print_delta(obj.run_duration) if obj.run_duration else "-"

    def ban_delay(self, obj): # pylint: disable=no-self-use
        """ Calculate ban_delay from worker creation to ban_date """

        result = '-'
        if obj.ban_date:
            result = str(obj.ban_date - obj.creation)
        return result
