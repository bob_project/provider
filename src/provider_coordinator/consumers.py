"""
Definition of Websocket server logic or how do we distribute
messages between computers and backend depending on their content.
"""

import json
import logging

from channels.db import database_sync_to_async
from channels.generic.websocket import AsyncWebsocketConsumer

logger = logging.getLogger('coordinator')

class CoordinatorConsumer(AsyncWebsocketConsumer):
    """ Websocket server definition for coordinator """

    @database_sync_to_async
    def connect_computer(self, computer): # pylint: disable=no-self-use
        """ Connect computer """

        computer.state = 'UP'
        computer.save()

    @database_sync_to_async
    def connect_backend(self, backend): # pylint: disable=no-self-use
        """ Connect backend """

        backend.connected = True
        backend.save()

    @database_sync_to_async
    def disconnect_computer(self, computer): # pylint: disable=no-self-use
        """ Disconnect computer """

        computer.refresh_from_db()
        computer.state = 'DOWN'
        computer.save()

    @database_sync_to_async
    def disconnect_backend(self, backend): # pylint: disable=no-self-use
        """ Disconnect backend """

        backend.connected = False
        backend.save()

    async def connect(self):
        """ Connect websocket client """

        self.room = "" # pylint: disable=attribute-defined-outside-init

        # connect computer
        if 'computer' in self.scope:
            logger.debug("Connect computer to websocket : {}".format(self.scope['computer']))
            self.room = self.scope['computer'].room # pylint: disable=attribute-defined-outside-init
            await self.connect_computer(self.scope['computer'])
         # connect backend
        elif 'backend' in self.scope:
            logger.debug("Connect backend worker to websocket : {}".format(self.scope['backend']))
            self.room = 'coordinator' # pylint: disable=attribute-defined-outside-init
            await self.connect_backend(self.scope['backend'])

        # register client in server's list
        if self.room:
            logger.debug("Register channel on server")
            await self.channel_layer.group_add(self.room, self.channel_name)
            await self.accept()

    async def disconnect(self, code): # pylint: disable=unused-argument
        """ Disconnect websoscket client """

        if self.room:
            # delete client from registered list
            await self.channel_layer.group_discard(
                self.room,
                self.channel_name
            )

        # disconnect computer
        if 'computer' in self.scope:
            logger.debug("Disconnect computer to websocket : {}".format(self.scope['computer']))
            await self.disconnect_computer(self.scope['computer'])
        # disconnect backend
        elif 'backend' in self.scope:
            logger.debug("Disconnect backend worker to websocket : {}".format(self.scope['backend']))
            await self.disconnect_backend(self.scope['backend'])

    async def receive(self, text_data=None, bytes_data=None):
        """
        Receive data from websocket connection

        Parameters:
            text_data: Raw data received
        """

        logger.debug("Receive message through websocket : {}".format(text_data))
        try:
            # format received data to json
            data = json.loads(text_data)

            # send message from backend to computer
            if 'backend' in self.scope and not data['type'] == 'STOP_BACKEND':
                await self.channel_layer.group_send(data["room"], {
                    'type': 'message',
                    'data': data
                })
            # send message from computer to backend
            elif 'computer' in self.scope and data['type'] == 'STOP_COMPUTER':
                await self.channel_layer.group_send(self.room, {
                    'type': 'message',
                    'data': data
                })
            elif 'computer' in self.scope:
                data['computer'] = self.room
                await self.channel_layer.group_send('coordinator', {
                    'type': 'message',
                    'data': data
                })
            # send back message to backend
            elif 'backend' in self.scope:
                await self.channel_layer.group_send('coordinator', {
                    'type': 'message',
                    'data': data
                })

        except Exception as exception:
            # send back exception to client who triggered it
            await self.channel_layer.group_send(self.room, {
                'type': 'error',
                'data': str(exception)
            })

    async def message(self, event):
        """ Sending message to websocket client """

        logger.debug("Send message through websocket : {}".format(event['data']))
        await self.send(text_data=json.dumps(event['data']))

    async def error(self, event):
        """ Return error to websocket client who triggered exception """

        logger.error("Error happenned while processing : {}".format(event['data']))
        await self.send(text_data=json.dumps({
            'type': 'ERROR',
            'message': 'Something went wrong while processing your message',
        }))
