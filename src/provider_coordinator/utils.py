"""
Utility functions for coordinator
"""

import logging
import xlsxwriter

from provider_backend.utils.utils import print_delta
from provider_coordinator.models import BatchWorker

logger = logging.getLogger('provider')

def create_stat_cheat(workbook, batch):
    """ Create a stats worksheet for a batch. """

    sheetname = "{}_{}_{}_{}".format(batch.project.slug.upper(), batch.mode_attribute, batch.mode_vote, batch.saboteur_rate)
    if batch.shadow:
        sheetname += '_SHADOW'

    sheet = workbook.add_worksheet(sheetname)
    stats = batch.stats()
    for index, key in enumerate(stats.keys()):
        sheet.write(index, 0, key)
        sheet.write(index, 1, stats[key])

    sheet.write(0, 3, "ID")
    sheet.write(0, 4, "Banned")
    sheet.write(0, 5, "Saboteur")
    sheet.write(0, 6, "Backtrack cost")
    sheet.write(0, 7, "Presence duration")
    sheet.write(0, 8, "Ban delay")

    index = 1
    for worker in BatchWorker.objects.filter(batch=batch):
        sheet.write(index, 3, worker.id)
        sheet.write(index, 4, worker.banned)
        sheet.write(index, 5, worker.saboteur)
        sheet.write(index, 6, worker.backtrack_cost)
        sheet.write(index, 7, print_delta(worker.run_duration))
        if worker.ban_date:
            sheet.write(index, 8, str(worker.ban_date - worker.creation))
        index += 1

def create_excel(batchs, path):
    """ Create an excel file with batchs stats. """

    workbook = xlsxwriter.Workbook(path)
    for batch in batchs:
        create_stat_cheat(workbook, batch)
    workbook.close()
