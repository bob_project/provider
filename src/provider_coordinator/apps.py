""" Provider coordinator app configuration """

from django.apps import AppConfig

class ProviderCoordinatorConfig(AppConfig):
    """ Application configuration """

    name = 'provider_coordinator'
