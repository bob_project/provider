"""
Definition of the interface between websocket backend client
and logic registerd for each job.
"""

import logging
import pydoc

from django.conf import settings
from django.utils import timezone

from channels.db import database_sync_to_async

from provider.sentry import catch_exception

from provider_backend.models import Task, Job
from provider_backend.scheduling.attributors import Attributor
from provider_backend.scheduling.voters import Voter
from provider_backend.scheduling.processors import Processor

logger = logging.getLogger('backend')

class Scheduler:
    """
    Define an interface between backend calls at each processing loop
    or when a websocket message is received.

    There are 3 functions callable : attribute(), process(message), vote()
    """

    def __init__(self, backend):
        self.backend = backend

        # initialise all possible attributors
        self.attributors = {}
        for key, attributor_path in settings.ATTRIBUTORS.items():
            lib = pydoc.locate(attributor_path)
            if Attributor in lib.__bases__:
                self.attributors[key] = lib()
            else:
                logger.error("Provided attributor doesn't have attribute(job) function : {} - {}".format(key, attributor_path))

        # initialise all possible processors
        self.processors = {}
        for key, processor_path in settings.PROCESSORS.items():
            lib = pydoc.locate(processor_path)
            if Processor in lib.__bases__:
                self.processors[key] = lib()
            else:
                logger.error("Provided processor doesn't have process(message) function : {} - {}".format(key, processor_path))

        # initialise all possible voters
        self.voters = {}
        for key, voter_path in settings.VOTERS.items():
            lib = pydoc.locate(voter_path)
            if Voter in lib.__bases__:
                self.voters[key] = lib()
            else:
                logger.error("Provided voter doesn't have vote(job) function : {} - {}".format(key, voter_path))

    @database_sync_to_async
    def attribute(self):
        """
        This function will create computations according to the policy registered
        for each job and return computations created.
        """

        logger.debug("Start task attribution")
        computations = []
        try:
            for job in Job.objects.filter(backend=self.backend, state__in=['TODO', 'DOING']):
                if job.mode_attribute in self.attributors and job.check_modes() and job.project.filled():

                    date_1 = timezone.now()
                    computations += self.attributors[job.mode_attribute].attribute(job)
                    date_2 = timezone.now()

                    job.refresh_from_db()
                    # check if job started
                    if computations and not job.start:
                        job.start = timezone.now()
                    # calculate attribute duration
                    job.attribute_duration += (date_2 - date_1).total_seconds()
                    job.save()
                else:
                    logger.error("Unknown attributor provided for job : {}".format(job))
        except Exception as exception:
            logger.error(exception)
            catch_exception(exception)
        return computations

    @database_sync_to_async
    def process(self, message):
        """
        This function will process each message received and update database
        according to the policy associated with the task in the message.
        """

        logger.debug("Process message : {}".format(message))
        if 'taskId' in message:
            task = Task.objects.filter(id=message['taskId']).first()
            if task:
                if task.job.mode_process in self.processors:
                    self.processors[task.job.mode_process].process(message)
                else:
                    logger.error("Unknown processor provided for job : {}".format(task.job))
            else:
                logger.warning("Unknown task ID retrieved from message")
        else:
            logger.warning("Can't process message without taskId in it")

    @database_sync_to_async
    def vote(self):
        """
        This function will make a vote on differents computation to decide
        what the final result for each task will be. When result has been chosen,
        all workers who were wrong are banned and returned to be warned.
        """

        cheaters = []
        logger.debug("Start task voting")
        try:
            for job in Job.objects.filter(backend=self.backend, state__in=['TODO', 'DOING']):
                if job.mode_vote in self.voters and job.check_modes() and job.project.filled():
                    date_1 = timezone.now()
                    new_cheaters = self.voters[job.mode_vote].vote(job)
                    date_2 = timezone.now()

                    job.refresh_from_db()
                    # don't send ban message if job use shadow ban
                    if not job.shadow:
                        cheaters += new_cheaters
                    # check if job finished
                    if all([hasattr(task, 'result') for task in job.task_set.filter(expected_hash__isnull=True)]):
                        job.state = 'DONE'
                        job.end = timezone.now()
                    # calculate vote duration
                    job.vote_duration += (date_2 - date_1).total_seconds()
                    job.save()
                else:
                    logger.error("Unknown voter provided for job : {}".format(job))
        except Exception as exception:
            logger.error(exception)
            catch_exception(exception)
        return cheaters
