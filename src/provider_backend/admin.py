"""
Administration configuration for backend
"""

import logging
import pydoc

from django.contrib import admin
from django.contrib.sessions.models import Session
from django.utils.safestring import mark_safe
from django.utils.text import Truncator

from provider_backend.models import BackendWorker, Worker, Task, Job, Computation, Result, Variable
from provider_backend.utils.creation import create_job_dataset
from provider_backend.utils.utils import print_delta

logger = logging.getLogger('provider')

admin.site.site_header = 'BOB'
admin.site.site_title = 'BOB'
admin.site.index_title = 'Administration'

admin.site.register(Session)

def delete_tasks(modeladmin, request, queryset): # pylint: disable=unused-argument
    """ Delete all Tasks of a Job.  """

    for job in queryset:
        job.task_set.all().delete()

delete_tasks.short_description = "Delete all Tasks"

def generate_default_dataset(modeladmin, request, queryset): # pylint: disable=unused-argument
    """ Generate a default Task set for each Job. """

    for job in queryset:
        create_job_dataset(job)

generate_default_dataset.short_description = "Generate a default Task dataset"

def reset_job(modeladmin, request, queryset): # pylint: disable=unused-argument
    """ A usefull admin command to reset a job for tests. """

    for job in queryset:
        job.reset()

reset_job.short_description = "Reset Job to original state"

def get_preview(file, project):
    """ Retrieve preview for corresponding project """

    html = ''
    if file.name and project.wrapper:
        html = pydoc.locate(project.wrapper)().preview(file)
    return html

@admin.register(Variable)
class AdminVariable(admin.ModelAdmin):
    """ Admin configuration for Variable model. """

    list_display = ('name', 'value')
    list_filter = ('name', 'value')
    ordering = ('name', 'value')
    search_fields = ('name', 'value')
    fields = ('name', 'value')

@admin.register(BackendWorker)
class AdminBackendWorker(admin.ModelAdmin):
    """ Admin configuration for BackendWorker model. """

    list_display = ('token', 'connected', 'stop', 'delay', 'url', 'max_retry', 'ping_interval')
    list_filter = ('connected', 'stop', 'delay', 'url', 'max_retry', 'ping_interval')
    ordering = ('token',)
    search_fields = ('token',)
    fields = ('token', 'connected', 'stop', 'delay', 'url', 'max_retry', 'ping_interval')

@admin.register(Worker)
class AdminWorker(admin.ModelAdmin):
    """ Admin configuration for Worker model. """

    list_display = (
        'id', 'verified', 'session_reduced', 'room_reduced', 'state', 'last_seen',
        'banned','saboteur', 'backtrack_cost', 'duration', 'ban_delay',
        'spot_check_number', 'credibility', 'token_reduced'
    )
    list_filter = ('state', 'banned', 'saboteur')
    ordering = ('session', 'room', 'spot_check_number', 'last_seen')
    search_fields = ('session', 'room',)
    fields = (
        'session', 'room', 'state', 'last_seen', 'banned', 'saboteur', 'token',
        'backtrack_cost', 'duration', 'creation', 'ban_date', 'spot_check_number'
    )
    readonly_fields = (
        'session', 'room', 'last_seen', 'backtrack_cost', 'duration',
        'creation', 'ban_date', 'spot_check_number'
    )

    def verified(self, obj): # pylint: disable=no-self-use
        """ Show if Worker is verified (has a token) """

        return not obj.token == ''
    verified.boolean = True

    def session_reduced(self, obj): # pylint: disable=no-self-use
        """ Show reduced session """

        return Truncator(obj.session).chars(10, truncate='...') if obj.session else '-'

    def room_reduced(self, obj): # pylint: disable=no-self-use
        """ Show reduced room """

        return Truncator(obj.room).chars(10, truncate='...')

    def token_reduced(self, obj): # pylint: disable=no-self-use
        """ Show reduced token """

        return Truncator(obj.token).chars(10, truncate='...') if obj.token else '-'

    def duration(self, obj): # pylint: disable=no-self-use
        """ Show worker run duration """

        return print_delta(obj.run_duration) if obj.run_duration else '-'

    def ban_delay(self, obj): # pylint: disable=no-self-use
        """ Show worker ban delay """

        result = '-'
        if obj.ban_date:
            result = str(obj.ban_date - obj.creation)
        return result

    def credibility(self, obj): # pylint: disable=no-self-use
        """ Show worker credibility """

        return obj.credibility()

@admin.register(Job)
class AdminJob(admin.ModelAdmin):
    """ Admin configuration for Job model. """

    list_display = (
        'id', 'description', 'configuration', 'stats'
    )
    list_filter = ('name', 'threshold', 'replications', 'project', 'mode_attribute', 'mode_process', 'mode_vote', 'spot_check_prob', 'shadow')
    ordering = ('name', 'threshold', 'replications', 'spot_check_prob', 'shadow')
    search_fields = ('name', 'threshold', 'replications', 'shadow')
    fields = (
        'name', 'description', 'mode_attribute', 'mode_process', 'mode_vote',
        'state', 'threshold', 'replications', 'spot_check_prob', 'shadow', 'project', 'backend', 'stats'
    )
    actions = (reset_job, generate_default_dataset, delete_tasks)
    readonly_fields = ('stats',)

    def backend_reduced(self, obj): # pylint: disable=no-self-use
        """ Show reduced backend """

        html = '-'
        if hasattr(obj, 'backend') and obj.backend:
            html = Truncator(obj.backend.token).chars(20, truncate='...')
        return html

    def completion(self, obj): # pylint: disable=no-self-use
        """ Show completion percentage """

        return mark_safe(obj.completion())

    def configuration(self, obj): # pylint: disable=no-self-use
        """ Show Job configuration """

        html_config = (
            "<table><tbody>"
            "<tr><td>Name</td><td>{}</td></tr>"
            "<tr><td>State</td><td>{}</td></tr>"
            "<tr><td>Replications</td><td>{}</td></tr>"
            "<tr><td>Threshold</td><td>{}</td></tr>"
            "<tr><td>Spot Check Probability</td><td>{}</td></tr>"
            "<tr><td>Shadow</td><td>{}</td></tr>"
            "<tr><td>Project</td><td>{}</td></tr>"
            "<tr><td>Mode attribute</td><td>{}</td></tr>"
            "<tr><td>Mode process</td><td>{}</td></tr>"
            "<tr><td>Mode vote</td><td>{}</td></tr>"
            "</tbody></table>"
        ).format(
            obj.name,
            obj.state,
            obj.replications,
            obj.threshold,
            obj.spot_check_prob,
            '<img src="/static/admin/img/icon-yes.svg" alt="True">' if obj.shadow else '<img src="/static/admin/img/icon-no.svg" alt="False">',
            obj.project,
            obj.mode_attribute,
            obj.mode_process,
            obj.mode_vote
        )
        return mark_safe(html_config)

    def stats(self, obj): # pylint: disable=no-self-use
        """ Show Job statistics """

        stats = obj.stats()
        html_stats = (
            "<table><tbody>"
            "<tr><td>Completion</td><td>{} %</td></tr>"
            "<tr><td>Job duration</td><td>{}</td></tr>"
            "<tr><td>Task duration</td><td>{}</td></tr>"
            "<tr><td>Computation duration</td><td>{}</td></tr>"
            "<tr><td>Vote duration</td><td>{}</td></tr>"
            "<tr><td>Attribute duration</td><td>{}</td></tr>"
            "<tr><td>Network load</td><td>{} octets</td></tr>"
            "<tr><td>Task Replications</td><td>{}</td></tr>"
            "<tr><td>Shadow count</td><td>{}</td></tr>"
            "<tr><td>Rerolls</td><td>{}</td></tr>"
            "</tbody></table>"
        ).format(
            stats["completion"],
            str(stats["job_duration"] if stats["job_duration"] else "-"),
            str(stats["task_duration"] if stats["task_duration"] else "-"),
            str(stats["computation_duration"] if stats["computation_duration"] else "-"),
            str(stats["vote_duration"] if stats["vote_duration"] else "-"),
            str(stats["attribute_duration"] if stats["attribute_duration"] else "-"),
            stats["global_load"],
            stats["task_replications"],
            stats["shadow_count"],
            stats["rerolls"],
        )
        return mark_safe(html_stats)

@admin.register(Task)
class AdminTask(admin.ModelAdmin):
    """ Admin configuration for Task model. """

    list_display = ('id', 'job_reduced', 'state', 'duration', 'backtrack_cost', 'data_reduced', 'preview', 'hash_reduced')
    list_filter = ('job', 'state')
    ordering = ('job', 'state', 'start',)
    search_fields = ('job', 'state')
    fields = ('job', 'state', 'start', 'backtrack_cost', 'data', 'expected_hash', 'meta_data', 'preview')
    readonly_fields = ('job', 'start', 'backtrack_cost', 'preview')

    def job_reduced(self, obj): # pylint: disable=no-self-use
        """ Show reduced Job """

        return Truncator(obj.job.name).chars(20, truncate='...')

    def data_reduced(self, obj): # pylint: disable=no-self-use
        """ Show short link to data """

        html = '-'
        if obj.data.name:
            html = '<a href="/media/{}">{}</a>'.format(obj.data, Truncator(obj.data).chars(30, truncate='...'))
        return mark_safe(html)

    def preview(self, obj): # pylint: disable=no-self-use
        """ Preview data """

        return get_preview(obj.data, obj.job.project)

    def hash_reduced(self, obj): # pylint: disable=no-self-use
        """ Show reduced hash of expected result """

        return Truncator(obj.expected_hash).chars(10, truncate='...') if obj.expected_hash else ''

    def duration(self, obj): # pylint: disable=no-self-use
        """ Show task duration """

        html = '-'
        seconds = obj.duration()
        if seconds:
            html = print_delta(seconds)
        return html

@admin.register(Computation)
class AdminComputation(admin.ModelAdmin):
    """ Admin configuration for Computation model. """

    list_display = ('id', 'task_id', 'job_reduced', 'worker_reduced', 'duration', 'state', 'shadow', 'result_reduced', 'hash_reduced', 'preview')
    list_filter = ('state', 'task', 'worker', 'start', 'end', 'shadow')
    ordering = ('task', 'worker', 'state', 'start', 'end', 'shadow')
    search_fields = ('task', 'worker', 'start', 'end', 'state')
    fields = ('task', 'worker', 'start', 'end', 'state', 'shadow', 'result', 'hash_result', 'preview')
    readonly_fields = ('preview', 'start', 'end', 'worker', 'task', 'hash_result')

    def task_id(self, obj): # pylint: disable=no-self-use
        """ Get associated task ID """

        return obj.task.id

    def worker_reduced(self, obj): # pylint: disable=no-self-use
        """ Show reduced worker """

        return Truncator(obj.worker.session.session_key).chars(10, truncate='...')

    def hash_reduced(self, obj): # pylint: disable=no-self-use
        """ Show reduced data hash """

        return Truncator(obj.hash_result).chars(10, truncate='...') if obj.hash_result else ''

    def job_reduced(self, obj): # pylint: disable=no-self-use
        """ Show reduced Job """

        return Truncator(obj.task.job.name).chars(20, truncate='...')

    def duration(self, obj): # pylint: disable=no-self-use
        """ Show duration """
        return print_delta(obj.duration())

    def result_reduced(self, obj): # pylint: disable=no-self-use
        """ Short link to result """

        html = ''
        if obj.result.name:
            html = '<a href="/media/{}">{}</a>'.format(obj.result, Truncator(obj.result).chars(20, truncate='...'))
        return mark_safe(html)

    def preview(self, obj): # pylint: disable=no-self-use
        """ Preview result data """

        return get_preview(obj.result, obj.task.job.project)

@admin.register(Result)
class AdminResult(admin.ModelAdmin):
    """ Admin configuration for Result model. """

    list_display = ('id', 'task_id', 'job_reduced', 'date', 'result_reduced', 'preview')
    list_filter = ('task', 'date')
    ordering = ('task', 'date')
    search_fields = ('task', 'date')
    fields = ('task', 'workers', 'date', 'result', 'preview')
    readonly_fields = ('preview', 'date', 'workers', 'task')

    def job_reduced(self, obj): # pylint: disable=no-self-use
        """ Show reduced Job """

        return Truncator(obj.task.job.name).chars(20, truncate='...')

    def task_id(self, obj): # pylint: disable=no-self-use
        """ Get associated task ID """

        return obj.task.id

    def preview(self, obj): # pylint: disable=no-self-use
        """ Preview result data """

        return get_preview(obj.result, obj.task.job.project)

    def result_reduced(self, obj): # pylint: disable=no-self-use
        """  Short link to result data """

        html = ''
        if obj.result.name:
            html = '<a href="/media/{}">{}</a>'.format(obj.result, Truncator(obj.result).chars(20, truncate='...'))
        return mark_safe(html)
