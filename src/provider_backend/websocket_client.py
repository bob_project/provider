"""
A WebSocket Client running worker scheduler wich will attributes tasks to workers,
send them work notifications and aggregate their results
"""

import asyncio
import json
import logging

from abc import ABC, abstractmethod

import websockets

from channels.db import database_sync_to_async

from provider.sentry import catch_exception

logger = logging.getLogger('backend')

class StopException(Exception):
    """ A StopException """

class WebSocketClient(ABC):
    """
    This client handle websocket connection

    Attributes:
        loop:    Asyncio loop to run async tasks
        backend: BackendWorker instance
        tasks:   Async tasks to run
    """

    def __init__(self, backend):

        self.backend = backend

        # asyncio init
        self.loop = asyncio.get_event_loop()
        self.tasks = []
        self.add_task(self.slave_task(self.master_receive, delay=False))

    def add_task(self, task):
        """ Add a task to websocket client"""

        self.tasks.append(asyncio.ensure_future(task))

    async def connect(self):
        """
        Check if client is connected to websocket and server, if not try to reconnect
        """

        result = False
        try:
            # check if websocket has been initialised
            if not hasattr(self, 'socket') or (hasattr(self, 'socket') and not self.socket.open): # pylint: disable=access-member-before-definition
                logger.info("Try to connect to server at : {}".format(self.backend.url))

                # start a new connection with authentication token
                self.socket = await websockets.connect( # pylint: disable=attribute-defined-outside-init
                    self.backend.url,
                    extra_headers={"Authorization": self.backend.token},
                    ping_interval=self.backend.ping_interval
                )
                logger.info("Connected to server through websocket")

                # client is connected
                result = True
            # check if websocket is connected
            elif hasattr(self, 'socket') and self.socket.open:
                result = True
        except websockets.exceptions.InvalidStatusCode:
            logger.error("Couldn't connect to server, plz check your redis is running")
        except:
            logger.error("Couldn't contact websocket server, plz check it's running")
        # if any check passed then client isn't connected
        return result

    async def stop(self):
        """
        Send a stop signal to websocket server
        """

        message = {'type': 'STOP_BACKEND'}
        await self.send(message)

    async def send(self, message, count=0):
        """ Send a message through websocket. """

        if count < self.backend.max_retry:
            try:
                # send message through websocket connection
                await self.socket.send(json.dumps(message))
                logger.info("Send following message : {}".format(message))
            except:
                logger.warning("Failed to send message, trying to reconnect")
                await asyncio.sleep(self.backend.delay)
                await self.connect()
                await self.send(message, count=count+1)
        else:
            logger.error("Couldn't send message : {}".format(message))

    async def check_stop(self):
        """
        Check if backend is authorised to run
        """

        if hasattr(self.backend, 'refresh_from_db'):
            await database_sync_to_async(self.backend.refresh_from_db)()
        if self.backend.stop:
            logger.info("Instance of backend worker marked as stopped")
        return self.backend.stop

    async def master_receive(self):
        """
        A background task to receive websocket messages
        """

        # wait for a message from websocket connection
        message = json.loads(await self.socket.recv())
        logger.info('Received following message : {}'.format(message))

        # check if the message is a stop signal
        if message['type'] == 'STOP_BACKEND' and not 'room' in message:
            raise StopException()
        await self.receive(message)

    @abstractmethod
    async def receive(self, message):
        """ Receive WebSocket messages """

    async def slave_task(self, function, repeat=True, delay=True):
        """
        A background task without priority for reconnection

        Parameters:
            function: Function to run in the loop
            repeat: Mark if function execution has to be repeated or not
            delay: Mark if need to sleep at the end of each loop
        """

        # initial sleep while connecting
        await asyncio.sleep(self.backend.delay)

        try:
            while not await self.check_stop():
                # check connection is alive
                if hasattr(self, 'socket') and self.socket.open:
                    try:
                        await function()
                        if not repeat:
                            break
                        if delay:
                            await asyncio.sleep(self.backend.delay)
                    except websockets.exceptions.ConnectionClosed:
                        logger.error('Connection with server closed')
                        await asyncio.sleep(self.backend.delay)
                    except StopException:
                        break
                else:
                    await asyncio.sleep(self.backend.delay)
        except Exception as exception:
            logger.error(exception)
            catch_exception(exception)

    async def master_task(self, function, repeat=True, delay=True):
        """
        A background task with priority for reconnection

        Parameters:
            function: Function to run in the loop
            repeat: Mark if function execution has to be repeated or not
            delay: Mark if need to sleep at the end of each loop
        """

        try:
            # check if backend is authorised to run
            while not await self.check_stop():
                # check for websocket connection
                if await self.connect():
                    try:
                        await function()
                        if not repeat:
                            break
                    except StopException:
                        break
                    if delay:
                        await asyncio.sleep(self.backend.delay)
                else:
                    await asyncio.sleep(self.backend.delay)
        except Exception as exception:
            logger.error(exception)
            catch_exception(exception)

        # check if still connected
        if await self.connect():
            # if yes then send a stop signal
            await self.stop()

    def run(self):
        """ Start backend threads """

        logger.info("Start running")
        if self.tasks:

            # start asyncio tasks
            logger.debug("Start tasks")
            self.loop.run_until_complete(asyncio.wait(self.tasks))

            # final check for websocket connection is closed
            logger.info("Close socket ...")
            if hasattr(self, 'socket'):
                self.loop.run_until_complete(asyncio.ensure_future(self.socket.close()))

            logger.info("Retrieve exceptions ....")
            # print exceptions occured in asyncio tasks
            for task in self.tasks:
                task.result()

            logger.info("Client stopped properly")
