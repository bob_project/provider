"""
Implementations of different types of task attributor and banning systems.
"""

import logging
import random

from abc import ABC, abstractmethod

from django.utils import timezone

from provider.sentry import catch_exception
from provider_backend.models import Computation, Task, Worker

logger = logging.getLogger('backend')

class Attributor(ABC):
    """
    General purpose class with a unified interface
    """

    def get_tasks(self, job): # pylint: disable=no-self-use
        """ Retrieve all available Tasks of a Job. """

        return Task.objects.filter(job=job, state__in=['TODO', 'DOING'], expected_hash__isnull=True)

    def get_workers(self): # pylint: disable=no-self-use
        """ Retrieve all available workers. """

        return Worker.objects.filter(state='UP')

    def get_computations(self): # pylint: disable=no-self-use
        """ Retrieve computations from task and exlude those with away worker. """

        all_computations = Computation.objects.all()
        # exlude computations with disconnected worker
        all_computations.filter(state__in=['TODO', 'DOING'], worker__state='DOWN').update(state='FORSAKEN')
        # exclude completed computations of banned worker
        all_computations.filter(worker__banned=True, state__in=['DONE', 'STORED']).update(state='BANNED')
        return all_computations

    def create_computation(self, task, worker): # pylint: disable=no-self-use
        """
        Create a new computation for attributed worker to task.
        """

        computation = None
        try:
            if not task.computation_set.count():
                task.start = timezone.now()
                task.save()

            computation = Computation(task=task, worker=worker)
            if worker.banned:
                computation.shadow = True
            computation.save()
            logger.info("Add {} to {}".format(computation, worker))

        except Exception as exception:
            logger.error(exception)
            catch_exception(exception)

        return computation

    def attribute(self, job):
        """
        Try to create computations for each task of a job and return created computations.
        """


        logger.debug("Search tasks in need of computation for job : {}".format(job))
        c_list = []
        try:
            # search a task available
            tasks = self.get_tasks(job)
            for task in tasks:
                if not hasattr(task, 'result'):
                    logger.debug("Try to create computations for {}".format(task))
                    computations = self.get_computations()

                    computation_count = computations.filter(
                        worker__banned=False,
                        task=task,
                        state__in=['TODO', 'DOING', 'DONE']
                    ).count()

                    # if task need more computations
                    if task.job.replications > computation_count:
                        # search a worker available
                        workers = self.get_workers()
                        # exclude banned worker if job is not shadow
                        if not task.job.shadow:
                            workers = workers.filter(banned=False)
                        for worker in workers:

                            affect = True
                            if worker.token and not job.need_spot_check_tasks():
                                affect = False

                            if affect:
                                already_worked_on_task = computations.filter(task=task, worker=worker).exclude(
                                    state__in=['REROLL', 'FORSAKEN']
                                ).exists()
                                worker_computation_count = computations.filter(state__in=['TODO', 'DOING'], worker=worker).count()
                                computation = self.attribute_to_worker(task, worker, already_worked_on_task, worker_computation_count)

                                if computation:
                                    c_list.append(computation)
        except Exception as exception:
            logger.error(exception)
            catch_exception(exception)
        return c_list

    @abstractmethod
    def attribute_to_worker(self, task, worker, already_worked_on_task, worker_computation_count):
        """ Look if a worker is available for a task and return created computation."""

class FIFOAttributor(Attributor):
    """ Simple FIFO task attributor. """

    def attribute_to_worker(self, task, worker, already_worked_on_task, worker_computation_count):

        computation = None
        if not already_worked_on_task and not worker_computation_count:
            computation = self.create_computation(task, worker)
        return computation

class SpotCheckAttributor(Attributor):
    """ Spot check task attributor. """

    def attribute_to_worker(self, task, worker, already_worked_on_task, worker_computation_count):

        computation = None
        if random.random() < task.job.spot_check_prob and not worker_computation_count:
            spot_check_task = Task.objects.filter(job=task.job, expected_hash__isnull=False).order_by('?').first()
            if spot_check_task:
                computation = self.create_computation(spot_check_task, worker)
        elif not already_worked_on_task and not worker_computation_count:
            computation = self.create_computation(task, worker)
        return computation
