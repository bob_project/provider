"""
Implement different ways of processing messages retrieved from Web Socket.

Different messages :

- START_COMPUTATION: order worker to start computation
- BAN_WORKER: worker is banned
- BACKEND_STOP: order backend to stop

- WASM_INIT: the wasm module is loaded/initialised
- DATA_INIT: the data required for the computation is downloaded
- COMPUTE_END: the computation is finished
- RESULTS_END: the result is uploaded to the server
"""

import logging

from abc import ABC, abstractmethod

from provider.sentry import catch_exception
from provider_backend.models import Computation, Worker

logger = logging.getLogger('backend')

class Processor(ABC):
    """ General Processor class to have a unified interface. """

    def process(self, message):
        """
        Dispatch retrieved messages to appropriated function.
        """

        try:
            # get global variables needed for processing
            worker = Worker.objects.get(room=message['worker'])
            computation = Computation.objects.filter(worker=worker, task__id=message['taskId']).first()

            if computation and worker:
                # dispatch messages
                if message['type'] == 'DATA_INIT':
                    self.data_init(message, worker, computation)
                elif message['type'] == 'WASM_INIT':
                    self.wasm_init(message, worker, computation)
                elif message['type'] == 'COMPUTE_END':
                    self.compute_end(message, worker, computation)
                elif message['type'] == 'RESULTS_END':
                    self.result_end(message, worker, computation)
                else:
                    logger.warning("Following message hasn't been processed, unknwon type : {}".format(message))
            else:
                logger.warning("Following message hasn't been processed, couldn't retrieve worker or computation associated : {}".format(message))
        except Exception as exception:
            logger.error(exception)
            catch_exception(exception)

    @abstractmethod
    def wasm_init(self, message, worker, computation):
        """ Process WASM_INIT messages. """

    @abstractmethod
    def data_init(self, message, worker, computation):
        """ Process DATA_INIT messages. """

    @abstractmethod
    def compute_end(self, message, worker, computation):
        """ Process COMPUTE_END messages. """

    @abstractmethod
    def result_end(self, message, worker, computation):
        """ Process RESULT_END messages. """

class SimpleProcessor(Processor):
    """ Default way of processing Web Socket messages. """

    def wasm_init(self, message, worker, computation):

        if worker.state != 'DOWN':
            worker.state = 'RUNNING'
            worker.save()

    def data_init(self, message, worker, computation):

        if computation.state == 'TODO':
            computation.state = 'DOING'
            computation.save()
        if computation.task.state == 'TODO' and not computation.task.expected_hash:
            computation.task.state = 'DOING'
            computation.task.save()
        if computation.task.job.state == 'TODO':
            computation.task.job.state = 'DOING'
            computation.task.job.save()

    def compute_end(self, message, worker, computation):
        pass

    def result_end(self, message, worker, computation):

        if worker.banned and computation.state != 'BANNED':
            computation.state = 'BANNED'
            computation.save()
        if worker.state != 'DOWN':
            worker.state = 'UP'
            worker.save()
