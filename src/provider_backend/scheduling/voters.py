"""
Implentations of different types of result votes
"""

import logging

from abc import ABC, abstractmethod

from django.core.files.base import ContentFile

from provider.sentry import catch_exception
from provider_backend.models import Computation, Task, Result, Variable
from provider_backend.utils.credibility import group_construct, sum_of_probas, get_group_cred

logger = logging.getLogger('backend')

class Voter(ABC):
    """ General Voter class to have a unified interface. """

    def generate_stats(self, computations): # pylint: disable=no-self-use
        """ Generate votes repartition from list of computations by file result hash. """

        stats = {}
        for computation in computations:
            # sort votes by hash of file result
            if computation.hash_result not in stats:
                stats[computation.hash_result] = [computation]
            else:
                stats[computation.hash_result].append(computation)
        return stats

    def generate_result(self, stats, task, correct_hash): # pylint: disable=no-self-use
        """ Create result object and update related objects. """

        logger.info("Generate result for task : {}".format(task))
        # copy 1st computation file to save as result
        logger.debug("Chosen computations : {}".format(stats[correct_hash]))
        data = ContentFile(stats[correct_hash][0].result.read())
        data.name = stats[correct_hash][0].result.name

        # create result object
        result = Result(task=task, result=data)
        result.save()

        task.state = 'DONE'
        task.save()

        # add workers to result and archive computations
        logger.debug("Archive computations for result : {}".format(result))
        for computation in stats[correct_hash]:
            result.workers.add(computation.worker)
            computation.state = 'STORED'
            computation.save()
        result.save()

    def reroll_wrong_votes(self, wrong_votes): # pylint: disable=no-self-use
        """
        When there is an equality in votes, delete computations
        with lower number of votes to reroll them.
        """

        for wrong_hash in wrong_votes:
            for computation in wrong_votes[wrong_hash]:
                computation.state = 'REROLL'
                computation.save()

    def ban_cheaters(self, wrong_votes): # pylint: disable=no-self-use
        """ Ban workers who didn't provide chosen result. """

        cheaters = []
        for wrong_hash in wrong_votes:
            for computation in wrong_votes[wrong_hash]:
                logger.info("Banning : {}".format(computation.worker))
                computation.worker.ban()
                cheaters.append(computation.worker)
        return cheaters

    def vote(self, job):
        """ Do a vote for each task of job with selected method. """

        logger.debug("Start voting for job : {}".format(job))
        cheaters = []
        try:
            for task in Task.objects.filter(job=job):
                # check if result already computed
                if not hasattr(task, 'result'):

                    logger.debug("Start voting for task : {}".format(task))

                    # pre-process computations (ie: update state of computations realised by banned worker)
                    computations = Computation.objects.filter(task=task, state='DONE', hash_result__isnull=False)
                    computations.filter(worker__banned=True).update(state='BANNED')
                    computations = computations.filter(worker__banned=False)

                    logger.debug("Generate stats for task : {}".format(task))
                    stats = self.generate_stats(computations)

                    logger.debug("Compute stats meta data for task : {}".format(task))
                    # correct_hash = hashes with the higher number of votes
                    correct_hash, new_cheaters = self.vote_task(task, computations, stats)
                    wrong_hash = {k : stats[k] for k in set(stats) - set(correct_hash)}
                    cheaters += new_cheaters

                    # if there is more than 1 hash with maxium number of votes reroll computations
                    if len(correct_hash) > 1:
                        logger.info("Reroll wrong votes for task : {}".format(task))
                        self.reroll_wrong_votes(wrong_hash)
                    elif len(correct_hash) == 1:
                        self.generate_result(stats, task, correct_hash[0])
                        cheaters += self.ban_cheaters(wrong_hash)
        except Exception as exception:
            logger.error(exception)
            catch_exception(exception)
        return cheaters

    @abstractmethod
    def vote_task(self, task, computations, stats):
        """ Vote for the correct hash with selected method. """

class MajorityVoter(Voter):
    """ Choose final result by voting between all computations affected to task. """

    def vote_task(self, task, computations, stats):

        correct_hash = []
        # wait for all computations to be completed
        if computations.count() >= task.job.replications:
            # max_nb_vote = maximum number of vote for the same number
            max_nb_vote = len(stats[max(stats, key= lambda k: len(stats[k]))])
            correct_hash = [filtered for filtered in filter(lambda k: len(stats[k]) == max_nb_vote, stats)] # pylint: disable=unnecessary-comprehension
        return correct_hash, []

class MFirstVoter(Voter):
    """ Choose final result by taking the first result returned m-times (m = job.threashold). """

    def vote_task(self, task, computations, stats):

        correct_hash = []
        # wait for all computations to be completed
        if computations.count() >= task.job.threshold:
            # max_nb_vote = maximum number of vote for the same number
            max_nb_vote = len(stats[max(stats, key= lambda k: len(stats[k]))])
            if max_nb_vote >= task.job.threshold:
                correct_hash = [filtered for filtered in filter(lambda k: len(stats[k]) == max_nb_vote, stats)] # pylint: disable=unnecessary-comprehension
        return correct_hash, []

class CredibilityVoter(Voter):
    """ Voter based on worker credibility throught regular spot-checking of worker results. """

    def vote_task(self, task, computations, stats):

        cheaters = []
        correct_hash = []
        # if task is a spot-check verify provided results
        if task.expected_hash:
            for computation in computations:
                # compare exptected and provided result
                if computation.hash_result != task.expected_hash:
                    # if comparison fail, worker get banned
                    logger.info("Banning : {}".format(computation.worker))
                    computation.worker.ban()
                    cheaters.append(computation.worker)
                else:
                    logger.info("Worker succeed it's spot-check : {}".format(computation.worker))
                    computation.worker.spot_check_number += 1
                    computation.worker.save()
                    computation.state = 'CHECKED'
                    computation.save()
        # start voting with credibility based system
        elif computations.exists():
            groups = group_construct(stats)
            probas_sum = sum_of_probas(groups)

            credibility = {}
            for result_hash in groups:
                credibility[result_hash] = get_group_cred(result_hash, groups, probas_sum)
            max_credibility = max(credibility.values())

            if max_credibility >= float(Variable.objects.get(name="THRESHOLD_CREDIBILITY").value):
                correct_hash = [filtered for filtered in filter(lambda k: credibility[k] == max_credibility, credibility)] # pylint: disable=unnecessary-comprehension
        return correct_hash, cheaters
