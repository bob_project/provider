"""
A WebSocket Client running worker scheduler wich will attributes tasks to workers,
send them work notifications and aggregate their results
"""

import logging
import os

from django.conf import settings
from django.core.management.base import BaseCommand

from channels.db import database_sync_to_async
from dotenv import load_dotenv

from provider.sentry import init

from provider_backend.utils.creation import init_backend
from provider_backend.scheduler import Scheduler
from provider_backend.websocket_client import WebSocketClient

logger = logging.getLogger('backend')

init()
load_dotenv(verbose=True, override=True, dotenv_path='.env')

class BackendClient(WebSocketClient):
    """
    This client handle interaction with Scheduler

    Attributes:
        tasks:      Async tasks to run
        scheduler:  Instance of main Scheduler
    """

    def __init__(self, backend):

        super().__init__(backend)
        self.scheduler = Scheduler(self.backend)

        self.add_task(self.master_task(self.attribute))
        self.add_task(self.slave_task(self.vote))

    @database_sync_to_async
    def get_urls(self, computation): # pylint: disable=no-self-use
        """ Retrieve obejcts URLS. """

        return computation.task.job.project.glue.url, computation.task.job.project.wasm.url, computation.task.data.url

    async def start(self, computation):
        """
        Send a notification to worker to start a new computation

        Parameters:
            computation: Computation attributed to worker
        """

        glue_url, wasm_url, data_url = await self.get_urls(computation)
        # create json message to send
        message = {
            'type': 'START_COMPUTATION',
            'glue': glue_url,
            'wasm': wasm_url,
            'worker': settings.WORKER_CODE,
            'data': data_url,
            'room': computation.worker.room,
            'id': computation.task.id,
        }
        await self.send(message)

    async def ban(self, worker):
        """ Send a stop signal to websocket server. """

        message = {'type': 'BAN_WORKER', 'room': worker.room}
        await self.send(message)

    async def receive(self, message):
        """ Receive all messages from websocket connection. """

        # make scheduler process message received
        await self.scheduler.process(message)

    async def attribute(self):
        """ Event loop which will call Scheduler.attribute(). """

        # retrieve all created computations
        c_list = await self.scheduler.attribute()
        # send comptations to workers
        for computation in c_list:
            await self.start(computation)

    async def vote(self):
        """ Event loop which will call Scheduler.vote(). """

        # ask scheduler to process results posted by workers
        cheaters = await self.scheduler.vote()
        for cheater in cheaters:
            await self.ban(cheater)

class Command(BaseCommand):
    """ A Django custom command """

    help = 'Start backend scheduler'

    def add_arguments(self, parser):
        parser.add_argument('--stop', action='store_true', help='Stop backend worker')

    def handle(self, *args, **options):

        try:
            # retrieve context variables
            token = os.environ.get("BACKEND_TOKEN")

            backend = init_backend(token, options)
            if not options['stop'] and backend:
                BackendClient(backend).run()
        except KeyboardInterrupt:
            logger.warning("Command interrupdated by keyboard")
