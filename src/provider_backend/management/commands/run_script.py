"""
A Debugging purpose command to run POC scripts
"""

import os
import sys
import argparse
from django.core.management.base import BaseCommand

class Command(BaseCommand):
    """ A Django custom command to run a script in Django context """

    help = 'Run script in current project environment'

    def add_arguments(self, parser):
        parser.add_argument('script', type=str, nargs=argparse.REMAINDER,
                            help='script file path and args')

    def handle(self, *args, **options):
        # get args
        args = options['script']
        script_path = args[0]

        # check file existence
        if not os.path.isfile(script_path):
            self.stderr.write(f'No such file: [{script_path}]')
            sys.exit(1)

        # set args
        sys_argv = [script_path] + args[1:]
        sys.argv = sys_argv

        # run
        with open(script_path, 'r') as file_handler:
            exec(file_handler.read(), {'__name__': '__main__'}) # pylint: disable=exec-used
