"""
Definition of models used for BBVC logic
"""

import datetime
import hashlib
import logging
import os
import secrets

from django.conf import settings
from django.contrib.sessions.models import Session
from django.db import models
from django.db.models.signals import post_delete, pre_save
from django.dispatch import receiver
from django.utils import timezone
from django.utils.text import Truncator

from provider_interface.models import Project

logger = logging.getLogger('provider')

def get_token():
    """ Return a token to fill default value. """
    return secrets.token_urlsafe(32)

def get_mode(setting_list):
    """
    Extract processors, voters and attributors registered in settings
    and create a models choices list.
    """

    modes = []
    for mode in setting_list:
        modes.append((mode, mode))
    return modes

class Variable(models.Model):
    """
    Global values used on other modules
    """

    name = models.CharField(max_length=100, unique=True)
    value = models.CharField(max_length=100)

    def __str__(self):
        return self.name

class BackendWorker(models.Model):
    """
    An instance of backend running on host.
    """

    token = models.CharField(max_length=50, default=get_token, unique=True)
    connected = models.BooleanField(default=False)
    stop = models.BooleanField(default=False)
    delay = models.IntegerField(default=5)
    url = models.CharField(max_length=50, default="ws://localhost:8000/backend/")
    start = models.DateTimeField(null=True, blank=True)
    max_retry = models.IntegerField(default=10)
    ping_interval = models.IntegerField(default=20)

    def __str__(self):
        return "<BackendWorker: {}>".format(self.token)

class Job(models.Model):
    """ A scientific project with multiple options for voting, processing and attributing. """

    JOB_STATE = (
        ('TODO', 'TODO'),
        ('DOING', 'DOING'),
        ('DONE', 'DONE'),
    )

    name = models.CharField(max_length=100, unique=True)
    description = models.TextField(max_length=1000, null=True, blank=True)
    backend = models.ForeignKey(BackendWorker, on_delete=models.SET_NULL, blank=True, null=True)
    state = models.CharField(max_length=10, choices=JOB_STATE, default=JOB_STATE[0][0])
    # global configuration
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    threshold = models.IntegerField(default=3)
    replications = models.IntegerField(default=5)
    shadow = models.BooleanField(default=False)
    spot_check_prob = models.FloatField(default=0.25)
    # computation configuration
    mode_attribute = models.CharField(max_length=50, choices=get_mode(settings.ATTRIBUTORS), default=get_mode(settings.ATTRIBUTORS)[0][0])
    mode_process = models.CharField(max_length=50, choices=get_mode(settings.PROCESSORS), default=get_mode(settings.PROCESSORS)[0][0])
    mode_vote = models.CharField(max_length=50, choices=get_mode(settings.VOTERS), default=get_mode(settings.VOTERS)[0][0])
    # statistics
    start = models.DateTimeField(null=True, blank=True)
    end = models.DateTimeField(null=True, blank=True)
    vote_duration = models.FloatField(default=0.0)
    attribute_duration = models.FloatField(default=0.0)

    def __str__(self):
        return "<Job: {}>".format(self.name)

    def completion(self):
        """ Return percentage of Task DONE. """

        result = 'N/A'
        task_set = self.task_set.filter(expected_hash__isnull=True)
        if task_set.count():
            result = round(task_set.filter(state='DONE').count() / task_set.count() * 100, 2)
        return result

    def reset(self):
        """ Reset Job to original state """

        for task in self.task_set.all():
            for computation in task.computation_set.all():
                try:
                    if not computation.worker.token:
                        computation.worker.delete()
                    computation.delete()
                except:
                    pass
            if hasattr(task, 'result'):
                task.result.delete()
            task.start = None
            task.replications = 0
            task.backtrack_cost = 0
            if not task.expected_hash:
                task.state = 'TODO'
            task.save()
        self.vote_duration = 0.0
        self.attribute_duration = 0.0
        self.start = None
        self.end = None
        self.state = 'TODO'
        self.save()

    def network_load(self):
        """ Calculate Job network load """

        worker_size = os.path.getsize("provider_backend/static/wasm_worker.js")

        global_load = 0
        for task in self.task_set.all():
            for computation in task.computation_set.all():
                if computation.result.name:
                    global_load += computation.result.size + task.data.size + worker_size
                    if computation.worker.saboteur:
                        global_load += task.job.project.glue_saboteur.size + task.job.project.wasm_saboteur.size
                    else:
                        global_load += task.job.project.glue.size + task.job.project.wasm.size
        return global_load

    def duration(self):
        """ Get Job duration """

        duration = 0
        if self.start and self.end:
            duration = (self.end - self.start).total_seconds()
        return duration

    def check_modes(self):
        """ Check Job mode_vote and attribute compatibility """

        check = self.mode_vote == 'CREDIB' and self.mode_attribute == 'SPOTCK' or (self.mode_vote != 'CREDIB' and self.mode_attribute != 'SPOTCK')
        if not check:
            logger.warning("mod_vote and mode_attribute aren't compatible for {}".format(self))
        return check

    def stats(self):
        """ Calculate Job statistics """

        task_replications = 0
        global_load = 0
        rerolls = 0
        shadow_count = 0
        task_duration_secs = 0
        computation_duration_secs = 0

        worker_size = os.path.getsize("provider_backend/static/wasm_worker.js")

        for task in self.task_set.all():
            computations = task.computation_set.all()
            task_replications += computations.count()
            rerolls += computations.filter(state="REROLL").count()
            task_duration_secs += task.duration()
            for computation in computations:
                computation_duration_secs += computation.duration()
                if computation.shadow:
                    shadow_count += 1
                if computation.result.name:
                    global_load += computation.result.size + task.data.size + worker_size
                    if computation.worker.saboteur:
                        global_load += task.job.project.glue_saboteur.size + task.job.project.wasm_saboteur.size
                    else:
                        global_load += task.job.project.glue.size + task.job.project.wasm.size

        task_duration = datetime.timedelta(seconds=task_duration_secs)
        computation_duration = datetime.timedelta(seconds=computation_duration_secs)

        return {
            "completion": self.completion(),
            "global_load": global_load,
            "task_replications": task_replications,
            "rerolls": rerolls,
            "shadow_count": shadow_count,
            "job_duration": datetime.timedelta(seconds=self.duration()),
            "task_duration": task_duration,
            "computation_duration": computation_duration,
            "vote_duration": datetime.timedelta(seconds=self.vote_duration),
            "attribute_duration": datetime.timedelta(seconds=self.attribute_duration),
        }

    def need_spot_check_tasks(self):
        """ Dermine if Job need more spot check Tasks """

        percentage = float(Variable.objects.get(name="SPOT_CHECK_TASK_PERCENTAGE").value)
        all_tasks = self.task_set.all()
        spot_check_number = int(all_tasks.count() * percentage) + 1
        already_done = all_tasks.filter(expected_hash__isnull=False).count()

        return spot_check_number - already_done > 0

def get_data_path(instance, filename): # pylint: disable=unused-argument
    """ Return path to Task data file. """

    filename_ext = os.path.splitext(filename)[1]
    return "data/{}{}".format(secrets.token_urlsafe(32), filename_ext)

def get_comput_path(instance, filename): # pylint: disable=unused-argument
    """ Return path to Computation result file. """

    filename_ext = os.path.splitext(filename)[1]
    return "comput/{}{}".format(secrets.token_urlsafe(32), filename_ext)

def get_result_path(instance, filename): # pylint: disable=unused-argument
    """ Return path to Result result file. """

    filename_ext = os.path.splitext(filename)[1]
    return "result/{}{}".format(secrets.token_urlsafe(32), filename_ext)

class Task(models.Model):
    """ A subpart of a Job with a dataset to process. """

    TASK_STATE = (
        ('TODO', 'TODO'),
        ('DOING', 'DOING'),
        ('DONE', 'DONE'),
        ('SPOT_CHECK', 'SPOT_CHECK'),
    )

    job = models.ForeignKey(Job, on_delete=models.CASCADE)
    state = models.CharField(max_length=10, choices=TASK_STATE, default=TASK_STATE[0][0])
    data = models.FileField(upload_to=get_data_path)
    meta_data = models.CharField(max_length=100, null=True, blank=True)
    start = models.DateTimeField(null=True, blank=True)
    backtrack_cost = models.IntegerField(default=0)
    expected_hash = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        return "<Task: id={}, job={}>".format(self.id, Truncator(self.job.name).chars(20, truncate='...'))

    def duration(self):
        """ Get Task duration """

        duration = 0
        if hasattr(self, 'result'):
            duration = (self.result.date - self.start).total_seconds() # pylint: disable=no-member
        return duration

class Computation(models.Model):
    """ A Task replication associated with a Worker. """

    COMPUTATION_STATE = (
        ('TODO', 'TODO'),
        ('DOING', 'DOING'),
        ('DONE', 'DONE'),
        ('STORED', 'STORED'),
        ('BANNED', 'BANNED'),
        ('REROLL', 'REROLL'),
        ('FORSAKEN', 'FORSAKEN'),
        ('CHECKED', 'CHECKED'),
    )

    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    worker = models.ForeignKey('Worker', on_delete=models.CASCADE)
    start = models.DateTimeField(default=timezone.now)
    end = models.DateTimeField(null=True, blank=True)
    state = models.CharField(max_length=10, choices=COMPUTATION_STATE, default=COMPUTATION_STATE[0][0])
    result = models.FileField(upload_to=get_comput_path, null=True, blank=True)
    hash_result = models.CharField(max_length=100, null=True, blank=True)
    shadow = models.BooleanField(default=False)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        """ Create result file hash when uploaded. """

        # check file is present
        if self.result.name:
            # Create hash
            logger.debug("Generate hash for computation result")
            hasher = hashlib.md5()
            self.result.seek(0)
            while True:
                buf = self.result.read(104857600)
                if not buf:
                    break
                hasher.update(buf)
            # save hash on instance
            self.hash_result = hasher.hexdigest()
            self.result.seek(0)
        super(Computation, self).save(force_insert=force_insert, force_update=force_update, using=using, update_fields=update_fields) # pylint: disable=super-with-arguments

    def __str__(self):
        return "<Computation: id={}, task={}, worker={}>".format(self.id, self.task, self.worker)

    def duration(self):
        """ Get Computation duration """

        duration = 0
        if self.start and self.end:
            duration = (self.end - self.start).total_seconds()
        return duration

class Worker(models.Model):
    """ A web browser volunteer doing computations when visiting our web pages. """

    WORKER_STATE = (
        ('UP', 'UP'),
        ('RUNNING', 'RUNNING'),
        ('DOWN', 'DOWN'),
    )

    session = models.OneToOneField(Session, on_delete=models.SET_NULL, null=True, blank=True)
    room = models.CharField(max_length=50, default=get_token, unique=True)
    state = models.CharField(max_length=10, choices=WORKER_STATE, default=WORKER_STATE[1][0])
    last_seen = models.DateTimeField(default=timezone.now)
    banned = models.BooleanField(default=False)
    saboteur = models.BooleanField(default=False)
    backtrack_cost = models.IntegerField(default=0)
    run_duration = models.FloatField(default=0.0)
    creation = models.DateTimeField(default=timezone.now)
    ban_date = models.DateTimeField(null=True, blank=True)
    spot_check_number = models.IntegerField(default=0)
    token = models.CharField(max_length=50, null=True, blank=True)

    def ban(self):
        """
        Ban a worker and retrieve all results and computations he participated, delete them
        to force recomputation and set banned field to True.
        """

        logger.debug("Ban {} and process backtrack".format(self))
        self.computation_set.all().update(state='BANNED')
        for result in self.result_set.all():
            result.task.job.state = 'DOING'
            result.task.job.save()
            result.task.state = 'DOING'
            self.backtrack_cost += result.workers.count()
            result.task.backtrack_cost += 1
            result.task.save()
            result.delete()
        self.banned = True
        self.ban_date = timezone.now()
        self.save()

    def credibility(self):
        """ Calculate Worker credibility """

        saboteur_rate = float(Variable.objects.get(name="ESTIMATED_SABOTEUR_RATE").value)
        credibility = 1 - saboteur_rate
        if self.spot_check_number:
            credibility = 1 - saboteur_rate / self.spot_check_number
        return credibility

    def __str__(self):
        return "<Worker: id={}, banned={}>".format(self.id, self.banned)

class Result(models.Model):
    """ A Task result after multiple Computations were compared. """

    task = models.OneToOneField(Task, on_delete=models.CASCADE)
    workers = models.ManyToManyField(Worker, blank=True)
    date = models.DateTimeField(default=timezone.now)
    result = models.FileField(upload_to=get_result_path)

    def __str__(self):
        return "<Result: id={}, task={}".format(self.id, self.task)

@receiver(post_delete, sender=Task)
def auto_delete_task_file_on_delete(sender, instance, **kwargs): # pylint: disable=unused-argument
    """ Deletes file from filesystem when corresponding `Task` object is deleted. """

    if instance.data:
        if os.path.isfile(instance.data.path):
            logger.debug("Delete old file : {}".format(instance.data.path))
            try:
                os.remove(instance.data.path)
            except:
                pass

@receiver(pre_save, sender=Task)
def auto_delete_task_file_on_change(sender, instance, **kwargs): # pylint: disable=unused-argument
    """ Deletes old file from filesystem when corresponding `Task` object is updated with new file. """

    if not instance.pk:
        return

    try:
        old = Task.objects.get(pk=instance.pk)
    except Task.DoesNotExist:
        return

    if old.data.name and not old.data == instance.data:
        if os.path.isfile(old.data.path):
            logger.debug("Delete old file : {}".format(instance.data.path))
            try:
                os.remove(old.data.path)
            except:
                pass

@receiver(post_delete, sender=Computation)
def auto_delete_computation_file_on_delete(sender, instance, **kwargs): # pylint: disable=unused-argument
    """ Deletes file from filesystem when corresponding `Computation` object is deleted. """

    if instance.result:
        if os.path.isfile(instance.result.path):
            logger.debug("Delete old file : {}".format(instance.result.path))
            try:
                os.remove(instance.result.path)
            except:
                pass

@receiver(pre_save, sender=Computation)
def auto_delete_computation_file_on_change(sender, instance, **kwargs): # pylint: disable=unused-argument
    """ Deletes old file from filesystem when corresponding `Computation` object is updated with new file. """

    if not instance.pk:
        return

    try:
        old = Computation.objects.get(pk=instance.pk)
    except Computation.DoesNotExist:
        return

    if old.result.name and not old.result == instance.result:
        if os.path.isfile(old.result.path):
            logger.debug("Delete old file : {}".format(instance.result.path))
            try:
                os.remove(old.result.path)
            except:
                pass

@receiver(post_delete, sender=Result)
def auto_delete_result_file_on_delete(sender, instance, **kwargs): # pylint: disable=unused-argument
    """ Deletes file from filesystem when corresponding `Result` object is deleted. """

    if instance.result:
        if os.path.isfile(instance.result.path):
            logger.debug("Delete old file : {}".format(instance.result.path))
            try:
                os.remove(instance.result.path)
            except:
                pass

@receiver(pre_save, sender=Result)
def auto_delete_result_file_on_change(sender, instance, **kwargs): # pylint: disable=unused-argument
    """ Deletes old file from filesystem when corresponding `Result` object is updated with new file. """

    if not instance.pk:
        return

    try:
        old = Result.objects.get(pk=instance.pk)
    except Result.DoesNotExist:
        return

    if old.result.name and not old.result == instance.result:
        if os.path.isfile(old.result.path):
            logger.debug("Delete old file : {}".format(instance.result.path))
            try:
                os.remove(old.result.path)
            except:
                pass
