"""
URL Routes for backend
"""

from django.urls import path

from provider_backend.views import worker, result, index

urlpatterns = [
	path('', index, name='Home and welcome page'),
	path('worker/', worker, name='Worker page to make computations'),
    path('result/<int:id_task>/', result, name='Posting result view'),
]
