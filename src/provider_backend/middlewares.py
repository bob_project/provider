"""
Definition of middlewares used in this project for worker and backend
authentication through WebSocket.
"""

import logging

from channels.db import database_sync_to_async
from django.contrib.sessions.models import Session
from django.utils import timezone

from provider.sentry import catch_exception
from provider_backend.models import BackendWorker, Worker
from provider_backend.utils.utils import convert
from provider_coordinator.models import Computer

logger = logging.getLogger('provider')

@database_sync_to_async
def get_computer(token):
    """" Retrieve Computer from token """

    return Computer.objects.filter(token=token).first()

@database_sync_to_async
def get_backend(token):
    """" Retrieve BackendWorker from token """

    return BackendWorker.objects.filter(token=token).first()

@database_sync_to_async
def get_worker(session_key, headers):
    """" Retrieve worker from websocket headers """

    logger.debug("Retrieve worker associated with session")
    session = Session.objects.get(session_key=session_key)

    worker = None
    if 'cookie' in headers and 'verified' in headers['cookie']:
        token = list(filter(lambda cookie: 'verified' in cookie, headers['cookie'].split(";")))[0].split('=')[1]
        previous = Worker.objects.filter(session=session).first()
        if previous and previous.token != token:
            previous.delete()
        # associate session with a verified worker

        worker = Worker.objects.get(token=token)
        worker.session = session
    if not worker:
        previous = Worker.objects.filter(session=session).exclude(token='').first()
        if previous:
            previous.session = None
            previous.save()
        # associate session with a classic worker
        worker, _ = Worker.objects.get_or_create(session=session, token='')
    if 'sec-websocket-protocol' in headers and headers['sec-websocket-protocol'] == 'saboteur':
        worker.saboteur = True
    worker.last_seen = timezone.now()
    worker.save()
    return worker

class WorkerWSMiddleware: # pylint: disable=too-few-public-methods
    """ Add worker to websocket scope context """

    def __init__(self, app):
        self.app = app

    async def __call__(self, scope, receive, send):

        try:
            # retrieve websocket header
            headers = convert(dict(scope['headers']))
            # check authorisation header and try to connect computer or backend
            if 'authorization' in headers:
                logger.debug("Retrieve computer associated with session")
                computer = await get_computer(headers['authorization'])

                if computer:
                    logger.debug("Add computer to scope context")
                    scope['computer'] = computer
                else:
                    logger.debug("Retrieve backend worker associated with session")
                    backend = await get_backend(headers['authorization'])
                    if backend:
                        logger.debug("Add backend worker to scope context")
                        scope['backend'] = backend
            # try to connect worker
            else:
                # if session does not exist save it
                logger.debug("Retrieve worker associated with session")
                if not scope['session'] or not scope['session'].session_key:
                    logger.debug("Create session")
                    await database_sync_to_async(scope['session'].save)()

                # associate session to worker and create one if necessary
                logger.debug("Add worker to scope context")
                scope['worker'] = await get_worker(scope['session'].session_key, headers)

        except Exception as exception:
            logger.error(exception)
            catch_exception(exception)

        return await self.app(scope, receive, send)

class WorkerHTTPMiddleware: # pylint: disable=too-few-public-methods
    """ Add worker to request context """

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        # filter for api views
        if request.path.startswith('/result/') and request.POST.get('room'):
            try:
                # retrieve worker from room id
                worker = Worker.objects.get(room=request.POST.get('room'))
                worker.last_seen = timezone.now()
                worker.save()

                logger.debug("Add worker to request context")
                request.worker = worker

            except Exception as exception:
                logger.error(exception)
                catch_exception(exception)

        return self.get_response(request)
