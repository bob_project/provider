/*
  This file is used to check for the user's consent and to be sure their browser
  supports all the technological stack we plan to use

  If a DOM interaction is made, it's here (ie: draw stuff on a canvas for
  demonstration purposes)
*/

/*
  Checking the saboteur status
*/
function isSaboteur() {
  if (document.cookie.split(';').some(function(item) {
    return item.trim().indexOf('saboteur=') == 0
  })) {
      if (document.cookie.split(';').some(function(item) {
          return item.indexOf('saboteur=YES') >= 0
      })) {
        console.log('[L] The cookie "saboteur" is YES')
        return true
      } else {
        console.log('[L] The cookie "saboteur" is NO')
        return false
      }
  } else {
    console.log('[L] The cookie "saboteur" doesn\'t exist')
    return false
  }
}

//===========================================================//
//                    GLOBAL VARIABLE SETUP                  //
//===========================================================//

//============= COOKIE WARN BOX CONFIGURATION ===============//
const COOKIE_WARN_BOW_STYLE = 'position:fixed;text-align:center;line-height:32px;width:100vw;background-color:#fe6d6d;color:#fff;font-size:16px;bottom: 0;left:0'
const COOKIE_WARN_IMAGE_STYLE = 'display:inline-block;height:100%;vertical-align:middle;margin-bottom:2px;margin-right:10px;'
const COOKIE_WARN_BUTTON_STYLE = 'border:none;margin:0;padding:0;width:auto;overflow:visible;background:transparent;color:inherit;font:inherit;line-height:normal;'
const COOKIE_WARN_MESSAGE = 'Do you agree to let BOB use your computer calculation power for scientific projects ?'

//================= GLOBAL CONFIGURATION ====================//
var PARTNER = false
if(location.host != 'localhost:8000' && location.host != 'provider.imotekh.fr') {
  PARTNER = true
}

const WORKER_URL_PATH = '/static/main_worker.js'
const SABOTEUR = isSaboteur()

var WS_PROTOCOL = 'ws'
if(location.protocol == 'https:') {
  WS_PROTOCOL = 'wss'
}

//==================== DEVELOPMENT URLS =====================//

var BASE_URL = location.protocol + '//' + location.host
var WS_URL = WS_PROTOCOL + '://' + location.host + '/backend/'

//===================== PRODUCTION URLS =====================//

if(PARTNER) {
  BASE_URL = 'https://provider.imotekh.fr'
  WS_URL = 'wss://provider.imotekh.fr/backend/'
}

//===========================================================//
//                         END BLOCK                         //
//===========================================================//

/*
  Consent is sexy.
*/
function askConsent() {
  console.log("[L] askConsent")
  // &#10004;&#65039; = ✔️
  // &#10060; = ❌
  document.body.innerHTML += `
    <div id='cookie-warn-box' style='${COOKIE_WARN_BOW_STYLE}'>
      <img src='${BASE_URL}/static/favicon.ico' width='28' style='${COOKIE_WARN_IMAGE_STYLE}'></img>
      ${COOKIE_WARN_MESSAGE}
      <button onclick='consent(true)' style='${COOKIE_WARN_BUTTON_STYLE}margin-left:10px;'>&#10004;&#65039;</button>
      <button onclick='consent(false)' style='${COOKIE_WARN_BUTTON_STYLE}'>&#10060;</button>
    </div>`
}

/*
  Checking the consent status
*/
function isConsentOK() {
  if (document.cookie.split(';').some(function(item) {
    return item.trim().indexOf('wasm-comp=') == 0
  })) {
      if (document.cookie.split(';').some(function(item) {
          return item.indexOf('wasm-comp=OK') >= 0
      })) {
        console.log('[L] The cookie "wasm-comp" is OK')
        return 'OK'
      } else {
         console.log('[L] The cookie "wasm-comp" is NOK')
        return 'NOK'
      }
  } else {
    console.log('[L] The cookie "wasm-comp" doesn\'t exist')
    return ''
  }
}

/*
  Get consent result of user and start worker if OK
*/
function consent(status) {
  if(status) {
    document.cookie = 'wasm-comp=OK;samesite=strict'
    init(WS_URL, BASE_URL, SABOTEUR)
  } else {
    document.cookie = 'wasm-comp=NOK;samesite=strict'
  }
  var elem = document.getElementById('cookie-warn-box')
  if(elem) {
    elem.parentNode.removeChild(elem);
  }
}

/*
  Returns a blob:// URL which points to a javascript file which will call
  importScripts with the given URL.
  https://stackoverflow.com/questions/21913673/execute-web-worker-from-different-origin
*/
function getWorkerURL(url) {
  const content = `importScripts("${url}");`
  return URL.createObjectURL(new Blob([content], {type: "text/javascript"}))
}

/*
  Creating a new worker to get on the job :)
*/
function loadMainWebWorker(wsURL, baseURL, workerURL, saboteur) {
  const worker_url = getWorkerURL(workerURL)
  let worker = new Worker(worker_url)
  URL.revokeObjectURL(worker_url)

  if(!PARTNER) {
    worker.addEventListener('message', (evt) => {
      if(evt.data.evtType == 'BANNED') {
        document.getElementById('ban').innerHTML = '&#10004;&#65039; (yes)'
      }
      else if(evt.data.evtType == 'RECEIVED_DATA') {
        var element = document.getElementById('task-data')
        element.setAttribute('href', evt.data.evtData)
        element.innerHTML = evt.data.evtData.substr(evt.data.evtData.length - 40);
      }
      else if(evt.data.evtType == 'INIT') {
        document.getElementById('connected').innerHTML = '&#10004;&#65039;'
      }
      else if(evt.data.evtType == 'DECONNECTED') {
        document.getElementById('connected').innerHTML = '&#10060; (yes)'
      }
      document.getElementById('status').innerHTML = evt.data.evtType
      if(evt.data.taskCount) {
        document.getElementById('task-count').innerHTML = evt.data.taskCount
      }
    })
  }
  // Start Worker
  worker.postMessage({
    evtType: "WORKER_INIT",
    wsURL: wsURL,
    baseURL: baseURL,
    saboteur: saboteur
  })
}

/*
  Check if consent is OK before launching worker
*/
function init(wsURL, baseURL, saboteur) {
  let consent = isConsentOK()
  if(consent == 'OK') {
    loadMainWebWorker(wsURL, baseURL, BASE_URL + WORKER_URL_PATH, saboteur)
  } else if(consent == '') {
    askConsent()
  }
}

init(WS_URL, BASE_URL, SABOTEUR)
