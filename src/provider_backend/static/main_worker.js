/*
  The role of this Web Worker is to deal with *all* the network exchanges
  Unloading them in a worker is cool because the main thread can do its thing
  and we don't screw with the user experience (#UXDesignIsMyPassion)

  Messages sent from this worker to the wasm_worker.js :
  WASM_INIT: please start the init process of the Wasm module
  COMPUTE: compute the cool stuff
*/

var taskCount = 0
/*
  Main class for a worker
*/
class WasmWorker {
  constructor(baseURL, saboteur, chatSocket, workerName, glueName, wasmName, roomName, taskId) {
    this.baseURL = baseURL
    this.saboteur = saboteur
    this.chatSocket = chatSocket
    this.workerName = workerName
    this.glueName = glueName
    this.wasmName = wasmName
    this.roomName = roomName
    this.taskId = taskId
    // Keep track of the messages being sent so we can resolve them correctly later
    this.evtId = 0
    this.idPromises = {}
  }

  /*
    Helper sending data over WS to the server
  */
  notifyServer(msgType) {
    console.log(`[MAIN] Notifying the server [${msgType}] [${this.taskId}]`)
    this.chatSocket.send(JSON.stringify({
      'type': msgType,
      'taskId': this.taskId,
    }))
    if(msgType == 'RESULTS_END') {
      console.log('================================================================')
    }
  }

  /*
    Returns a blob:// URL which points to a javascript file which will call
    importScripts with the given URL.
    https://stackoverflow.com/questions/21913673/execute-web-worker-from-different-origin
  */
  getWorkerURL(url) {
    const content = `importScripts("${url}");`
    return URL.createObjectURL(new Blob([content], {type: "text/javascript"}))
  }

  /*
    Initializing the worker, returns a promise resolved when is the wasm is also
    loaded/ready.
  */
  initialise() {
    return new Promise((resolve, reject) => {
      console.log('[MAIN] Initialising the web worker')
      const worker_url = this.getWorkerURL(this.baseURL + this.workerName)
      this.worker = new Worker(worker_url)
      URL.revokeObjectURL(worker_url)

      this.worker.postMessage({
        evtType: "WASM_INIT",
        evtData: {
          glueName: this.glueName,
          wasmName: this.wasmName,
          baseURL: this.baseURL,
          saboteur: this.saboteur
        }
      })
      this.worker.addEventListener('message', (evt) => {
        const { evtType, evtData, evtId } = evt.data
        if(evtType === "WASM_INIT") {
          this.notifyServer('WASM_INIT')
          resolve() // the init part is done, resolving the promise
        } else if(evtType === "DATA_INIT") {
          this.notifyServer('DATA_INIT')
        } else if(evtType === "COMPUTE_END") {
          this.notifyServer('COMPUTE_END')
        } else if(evtType === "RESULTS_END") {
          this.notifyServer('RESULTS_END')
          this.resolveResult(evtType, evtData, evtId)
          taskCount++
        } else {
          this.notifyServer('ERROR')
          // TODO: rejectError should probably not be triggered everytime
          // ie when the error handling system reports one on WASM_INIT
          this.rejectError(evtType, evtData, evtId)
        }
        evt.data.taskCount = taskCount
        self.postMessage(evt.data)
      }) // addEventListener
    }) // promise
  }

  /*
    Generic compute function contacting the worker
  */
  compute(dataPath) {
    return new Promise((resolve, reject) => {
      let id = this.evtId // micro optimisations are bad but it feels so good :c
      this.worker.postMessage({
        evtType: "COMPUTE",
        evtData: {
          'dataPath': dataPath,
          'taskId': this.taskId, // the task id is needed for the POST API request
          'roomName': this.roomName,
          'baseURL': this.baseURL
        },
        evtId: id,
      })
      self.postMessage({evtType: 'RECEIVED_DATA', evtData: dataPath})

      this.idPromises[id] = { resolve, reject }
      this.evtId++
    }) // Promise
  }

  /*
    Function called when a message dealing with a promise is recieved
    from the worker.
    Can be used for recieving the data or getting back the results
  */
  resolveResult(evtType, evtData, evtId) {
    if (evtId !== undefined && this.idPromises[evtId]) {
        this.idPromises[evtId].resolve(evtData)
        delete this.idPromises[evtId]
    }
  }

  /*
    Function called when an error message is recieved from the worker
  */
  rejectError(evtType, evtData, evtId) {
    if (evtId !== undefined && this.idPromises[evtId]) {
        this.idPromises[evtId].reject(evtData)
        delete this.idPromises[evtId]
    }
  }
}

function startWS(wsURL, baseURL, saboteur) {
  /*
    WebSocket stuff
  */

  var option = 'honest'
  if(saboteur) {
    option = 'saboteur'
  }
  socket = new WebSocket(wsURL, option)
  self.postMessage({evtType: "INIT"})
  socket.onmessage = function(e) {
    /*
    {
      'type':'start',
      'worker': [address],
      'glue': [address],
      'wasm': [address],
      'data': [address],
      'id': [id]
    }
    */
    let data = JSON.parse(e.data)

    let w // external definition so we can use it later on, when initialised

    if(data.type == 'START_COMPUTATION') {
      self.postMessage({evtType: "STARTED"})

      /* Creating a new worker based on the parameters */
      console.log("[MAIN] Received START_COMPUTATION", data)

      // The start message is the same for the first or n-time when getting a task
      // Checking if the worker is already created, and if the worker/glue have
      // changed according to the previous values
      let promises = []
      if(!w || data.worker != w.workerName || data.glue != w.glueName) {
        console.log("[MAIN] Creating a new worker")
        w = new WasmWorker(baseURL, saboteur, socket, data.worker, data.glue, data.wasm, data.room, data.id)
        promises.push(w.initialise())
      }

      // if the promises array is empty, this resolves instantly :)
      Promise.all(promises).then(() => {
        // at this point, we can start the computation logic
        w.compute(data.data).then(() => {
          self.postMessage({
              evtType: "ENDED"
          })
        })
      })
    } else if(data.type == "BAN_WORKER") {
      console.log("[MAIN] Worker banned, sorry.")
      self.postMessage({
          evtType: "BANNED"
      })
    } else {
      console.error("[MAIN] Unknown type of message", data)
    }
  }

  socket.onclose = function(event) {
    if(!event.wasClean) {
      self.postMessage({evtType: "DECONNECTED"})
      setTimeout(startWS(wsURL, baseURL, saboteur), 1000)
    }
  }
}

self.addEventListener('message', function(event) {
  if (event.data.evtType === "WORKER_INIT") {
    startWS(event.data.wsURL, event.data.baseURL, event.data.saboteur)
  }
}, false)
