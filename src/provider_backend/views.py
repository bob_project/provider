"""
Definition of workers API views
"""

import logging

from django.conf import settings
from django.http import HttpResponse
from django.shortcuts import render
from django.utils import timezone
from django.views.decorators.clickjacking import xframe_options_exempt
from django.views.decorators.csrf import csrf_exempt

from provider.sentry import catch_exception
from provider_backend.models import Computation

logger = logging.getLogger('provider')

def index(request):
    """ Welcome page to dispatch between honest and saboteur worker. """

    return render(request, 'provider_backend/index.html')

@xframe_options_exempt
def worker(request):
    """ View to handle worker request """

    context = {
        'debug': settings.DEBUG,
        'saboteur': 'true' if 'saboteur' in request.GET else 'false',
        'ignore': 'true' if 'ignore' in request.GET else 'false',
        'sentry_dsn': settings.SENTRY_DSN if hasattr(settings, 'SENTRY_DSN') else '',
    }
    if 'token' in request.GET:
        context['token'] = request.GET['token']
    return render(request, 'provider_backend/worker.html', context)

@csrf_exempt
def result(request, id_task):
    """ Handle computation results """

    status = 500
    try:
        # check view can be triggered
        if request.method == 'POST' and request.FILES.get('data') and hasattr(request, 'worker'):

            # retrieve computation
            computation = Computation.objects.filter(task__id=id_task, worker=request.worker, state__in=['TODO', 'DOING', 'DONE']).first()
            if computation:
                logger.info("Retrieving {} for results".format(computation))

                # register result data and computation state
                computation.result = request.FILES['data']
                status = 200
                if request.worker.banned:
                    computation.state = 'BANNED'
                    if not computation.shadow:
                        status = 403
                else:
                    computation.state = 'DONE'
                computation.end = timezone.now()
                computation.save()

                if computation.worker.state == 'RUNNING':
                    computation.worker.state = 'UP'
                    computation.worker.save()

                if request.worker.token:
                    if computation.task.job.need_spot_check_tasks():
                        computation.task.expected_hash = computation.hash_result
                        computation.task.state = 'SPOT_CHECK'
                        computation.task.save()
                    computation.delete()
            else:
                status = 403
        else:
            status = 403
    except Exception as exception:
        logger.error(exception)
        catch_exception(exception)
        status = 500

    return HttpResponse(status=status)
