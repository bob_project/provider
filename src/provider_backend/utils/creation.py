"""
Utility functions for Task creation
"""

import logging
import pydoc
import shutil

from os import listdir, makedirs
from os.path import join, isdir

from django.conf import settings
from django.core.files.base import ContentFile

from provider_backend.models import Task, Job, BackendWorker

logger = logging.getLogger('backend')

def create_task(job, path):
    """
    Create a task from Job object and data path
    """

    # copy data
    file = open(path, 'rb')
    data = ContentFile(file.read())
    data.name = file.name
    file.close()
    # create task object
    task = Task(job=job, data=data)
    task.save()
    logger.debug("Created task with data : {}".format(data.name))

def create_tasks(job, folder):
    """
    Create a bunch of Tasks from a folder path and a Job object
    """

    if isdir(folder):
        for filename in listdir(folder):
            create_task(job, join(folder, filename))
    else:
        logger.error("Provided data directory isn't a folder")

def create_default_job(project):
    """
    Create a default Job with provided project
    """

    backend = BackendWorker.objects.filter(url="ws://localhost:8000/backend/").first()

    job = Job(
        project=project,
        name=project.name,
        backend=backend,
    )
    job.save()
    create_job_dataset(job)
    return job

def create_job_dataset(job):
    """
    Create Job dataset
    """

    if job.project.wrapper:
        if isdir(settings.DEFAULT_DATASET_FOLDER):
            shutil.rmtree(settings.DEFAULT_DATASET_FOLDER)
        makedirs(settings.DEFAULT_DATASET_FOLDER)

        logger.debug("Create Tasks with project : {}".format(job.project))
        pydoc.locate(job.project.wrapper)().generate_dataset()
        create_tasks(job, settings.DEFAULT_DATASET_FOLDER)
        job.state = 'TODO'
        job.save()

        if isdir(settings.DEFAULT_DATASET_FOLDER):
            shutil.rmtree(settings.DEFAULT_DATASET_FOLDER)
    else:
        logger.error("Provided job's project has no wrapper : {}".format(job.project))

def init_backend(token, options):
    """ Retrieve backend worker from token and initialize it. """

    backend = None
    try:
        backend = BackendWorker.objects.get(token=token)

        if options['stop'] and backend.connected:
            logger.info("Signal backend to stop")
            backend.stop = True
        # set backend
        elif options['stop'] and not backend.connected:
            logger.info("Can't stop a non-running backend")
            backend.stop = False
        # start backend client
        else:
            # set default backend stop signal to False
            if backend.stop:
                backend.stop = False
        backend.save()
    except BackendWorker.DoesNotExist:
        logger.error("Failed to find backend instance, please check you token")
    return backend
