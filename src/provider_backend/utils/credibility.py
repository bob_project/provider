"""
Utility functions for credibility calculation
"""

def group_construct(stats):
    """ Construct groups """

    groups = {}
    for result_hash in stats:
        for computation in stats[result_hash]:
            if not result_hash in groups:
                groups[result_hash] = [computation.worker.credibility()]
            else:
                groups[result_hash].append(computation.worker.credibility())
    return groups

def p_gx_good(groupe_x):
    """
    CrG(Ga)

    groupe_x : a single group
    """

    cred_result = 0
    for index, current_cred_result in enumerate(groupe_x):
        if index == 0:
            cred_result = current_cred_result
        else:
            cred_result = cred_result * current_cred_result
    return cred_result

def product_p_gx_bad(groupe_x):
    """
    groupe_x : a set of groups
    returns the product of the bad probability
    """

    bad_cred_results = []
    for group in groupe_x:
        cred_result = 0
        for index, current_cred_result in enumerate(group):
            if index == 0:
                cred_result = 1 - current_cred_result
            else:
                cred_result = cred_result * (1 - current_cred_result)

        bad_cred_results.append(cred_result)

    product_bad_cred = 1
    for bad_cred_result in bad_cred_results:
        product_bad_cred *= bad_cred_result

    return product_bad_cred


def good_product_bad(groupe_j, groupe_not_j):
    """
    groupe_j : the current studied group
    groupe_not_j : *all* the other groups
    """

    # cred_result <=> P(groupe_j good)
    cred_result = p_gx_good(groupe_j)
    # Product of P(Gi bad)
    product_bad_cred = product_p_gx_bad(groupe_not_j)
    # Final product between P(groupe_j good) and Product of P(Gi bad)
    return cred_result * product_bad_cred

def sum_of_probas(groups):
    """ Calculate sum of probas """

    proba_sum = 0
    for result_hash in groups:
        group = groups[result_hash]
        not_group = []
        for tmp_hash in groups:
            if tmp_hash != result_hash:
                not_group.append(groups[tmp_hash])
        proba_sum += good_product_bad(group, not_group)
    return proba_sum

def get_group_cred(result_hash, groups, proba_sum):
    """ Get credibility for a group """

    group = groups[result_hash]
    not_group = []
    group_creds = [groups[result_hash] for result_hash in groups]
    for tmp_hash in groups:
        if tmp_hash != result_hash:
            not_group.append(groups[tmp_hash])
    return p_gx_good(group) * product_p_gx_bad(not_group) / (product_p_gx_bad(group_creds) + proba_sum)
