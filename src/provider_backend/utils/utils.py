"""
Utility methods for backend
"""

import datetime
import logging

from discord_webhook import DiscordWebhook, DiscordEmbed

logger = logging.getLogger('coordinator')

def print_delta(delta):
    """ Print an amount of seconds to timedelta. """

    return str(datetime.timedelta(seconds=delta))

def convert(data):
    """ Decode data """

    if isinstance(data, bytes):
        return data.decode('ascii')
    if isinstance(data, dict):
        return dict(map(convert, data.items()))
    if isinstance(data, tuple):
        return map(convert, data)
    return data

class WebhookManager:
    """ A simple wrapper to send messages to Discord webhook. """

    def __init__(self, url, name, silent):
        self.url = url
        self.name = name
        self.silent = silent

    def status(self, batch, state="START", title=""):
        """ Send batch status (START or END) with corresponding informations. """

        embed = DiscordEmbed(title=state + " " + title, color=242424)
        if state == "START":
            embed.add_embed_field(name="Project", value=batch.project.name)
            embed.add_embed_field(name="Saboteur Rate", value=str(batch.saboteur_rate))
            embed.add_embed_field(name="Mode Attribute", value=str(batch.mode_attribute))
            embed.add_embed_field(name="Mode Vote", value=str(batch.mode_vote))
            embed.add_embed_field(name="Shadow", value='YES' if batch.shadow else 'NO')
        elif state == "END":
            embed.add_embed_field(name="Completion", value="{} %".format(batch.completion))
            embed.add_embed_field(name="Network load", value="{} octets".format(batch.network_load))
            embed.add_embed_field(name="Task replications", value=str(batch.task_replications))
            embed.add_embed_field(name="Rerolls", value=str(batch.rerolls))
            embed.add_embed_field(name="Shadow count", value=str(batch.shadow_count))
            embed.add_embed_field(name="Job duration", value=print_delta(batch.job_duration))
            embed.add_embed_field(name="Task duration", value=print_delta(batch.task_duration))
            embed.add_embed_field(name="Computation duration", value=print_delta(batch.computation_duration))
            embed.add_embed_field(name="Vote duration", value=print_delta(batch.vote_duration))
            embed.add_embed_field(name="Attribute duration", value=print_delta(batch.attribute_duration))

        self._send(embed=embed)

    def result(self, file):
        """" Send result XLSX. """

        self._send(file=file)

    def error(self, message):
        """" Send an error message. """

        embed = DiscordEmbed(title=message, color=0xb91207)
        self._send(embed=embed)

    def _send(self, embed=None, file=None):
        """ Send embed to webhook. """

        # check can send message
        if not self.silent:
            if self.url:
                webhook = DiscordWebhook(url=self.url)
                if embed:
                    embed.set_footer(text=self.name)
                    embed.set_timestamp()
                    webhook.add_embed(embed)
                if file:
                    with open(file, "rb") as file_handler:
                        webhook.add_file(file=file_handler.read(), filename=file.replace('/', '-'))
                webhook.execute()
            else:
                logger.warning("Webhook URL is empy")
