"""
Utility user input methods
"""

import logging
import sys

from os.path import isfile

logger = logging.getLogger('backend')

def get_mode(setting_list):
    """
    Extract processors, voters and attributors registered in settings and create a list.
    """

    return list(setting_list.keys())

def get_choice_list(choices):
    """ Return choices key list. """

    return [key for key, value in choices]

def get_input(message, mandatory=False, is_int=False, default=None, is_file=False): # pylint: disable=too-many-branches
    """
    Get user input with multiple checks :

    Parameters:
        message: Display message to user
        mandatory: Set to True if it's required
        is_int: Set to True if need to retrieve and integer
        default: Set default value if entry is empty
        is_file: Specify if entry is a file path
    """

    message_str = message
    if mandatory:
        message_str += "*"
    if default:
        message_str += "(default={})".format(default)
    message_str += " : "
    entry = None
    if mandatory:
        while not entry:
            try:
                if is_int:
                    entry = int((input(message_str) or default))
                else:
                    entry = input(message_str) or default

                if is_file and not isfile(entry):
                    logger.error("Invalid file please retry")
                    entry = None
                elif not entry:
                    logger.error("Entry is empty please retry")
            except KeyboardInterrupt:
                sys.exit(0)
            except:
                logger.error("Entry is invalid please retry")
    else:
        if is_int:
            try:
                entry = int((input(message_str) or default))
            except KeyboardInterrupt:
                sys.exit(0)
            except:
                logger.error("Entry is invalid set to default={}".format(default))
                entry = default
        else:
            entry = input(message_str) or default
    return entry

def get_input_choices(message, choices):
    """
    Let user choose in a list of possible values.

    Parameters:
        message: Display message
        choices: List of possible values
    """

    result = None
    if len(choices) > 1:
        print(message)
        for index, mode in enumerate(choices, start=1):
            print("{} : {}".format(index, mode))
        chosen = None
        while not chosen:
            chosen = get_input("> ", is_int=True, mandatory=True)
            if chosen <= 0 or chosen > len(choices):
                chosen = None
                logger.error("Entry is invalid please retry")
        result = choices[chosen-1]
    else:
        result = choices[0]
    return result
