""" Provider backend app configuration """

from django.apps import AppConfig

class ProviderBackendConfig(AppConfig):
    """ Application configuration """

    name = 'provider_backend'
