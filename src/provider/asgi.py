"""
This module create an ASGI application
"""
# pylint: disable=wrong-import-position,wrong-import-order,ungrouped-imports

import os

from django.core.asgi import get_asgi_application
from provider.sentry import init, create_app

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'provider.settings.production')

init()
django_asgi_app = create_app(get_asgi_application())

from channels.auth import AuthMiddlewareStack
from channels.routing import ProtocolTypeRouter, URLRouter

from provider_backend.middlewares import WorkerWSMiddleware
from provider_backend.models import Variable
from provider.routes import websocket_urlpatterns

DEFAULT_VARIABLES = {
    "THRESHOLD_CREDIBILITY": "0.98",
    "ESTIMATED_SABOTEUR_RATE": "0.1",
    "DEFAULT_PNG_NUMBER": "10",
    "DEFAULT_PNG_SIZE": "500",
    "DEFAULT_TO_FOUND": "pass",
    "DEFAULT_HASH_WORD_MIN_SIZE": "4",
    "DEFAULT_HASH_WORD_MAX_SIZE": "4",
    "DEFAULT_HASH_ATTEMPTS": "7",
    "RATE_SHORT_WORKER": "0.8",
    "SABOTEUR_RATES": "0, 0.05, 0.1, 0.2, 0.3, 0.4, 0.5",
    "BATCH_TIMEOUT": "10",
    "SPOT_CHECK_TASK_PERCENTAGE": "0.05",
}

for variable in DEFAULT_VARIABLES:
    if not Variable.objects.filter(name=variable).exists():
        Variable(name=variable, value=DEFAULT_VARIABLES[variable]).save()

application = ProtocolTypeRouter({
    "http": django_asgi_app,
    "websocket": AuthMiddlewareStack(WorkerWSMiddleware(
        URLRouter(websocket_urlpatterns)
    )),
})
