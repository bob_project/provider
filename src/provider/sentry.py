"""
This module help to configure sentry for production environnement
"""

import sentry_sdk
from sentry_sdk import capture_exception
from sentry_sdk.integrations.django import DjangoIntegration
from sentry_sdk.integrations.asgi import SentryAsgiMiddleware

from django.conf import settings

def init():
    """ Initialise sentry SDK """

    if not settings.DEBUG and hasattr(settings, 'SENTRY_DSN'):
        sentry_sdk.init(dsn=settings.SENTRY_DSN, integrations=[DjangoIntegration(transaction_style='url')])

def create_app(app):
    """ Create sentry ASGI Middleware """


    if not settings.DEBUG and hasattr(settings, 'SENTRY_DSN'):
        app = SentryAsgiMiddleware(app)
    return app

def catch_exception(exception):
    """ Catch exception using Sentry """

    if not settings.DEBUG and hasattr(settings, 'SENTRY_DSN'):
        capture_exception(exception)
