"""
La configuration à utiliser pour les environnements de production
"""

from provider.settings.base import *

from dotenv import load_dotenv

load_dotenv(verbose=True, override=True, dotenv_path='.env')

SECRET_KEY = os.environ.get('TOKEN_PROD')
SENTRY_DSN = os.environ.get('SENTRY_DSN')

DEBUG = False

ALLOWED_HOSTS = ['provider.imotekh.fr']
CORS_ALLOWED_ORIGINS = ['https://provider.imotekh.fr', 'https://imotekh.fr']

DATABASES = {
    'default': {
        'ENGINE':   'django.db.backends.postgresql_psycopg2',
        'NAME':     os.environ.get('DB_NAME'),
        'USER':     os.environ.get('DB_USER'),
        'PASSWORD': os.environ.get('DB_PASSWORD'),
        'HOST':     os.environ.get('DB_HOST'),
        'PORT':     os.environ.get('DB_PORT'),
    }
}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '[{asctime}][{levelname}]: {module} {process:d} {thread:d} {message}',
            'datefmt' : '%d/%m/%Y %H:%M:%S',
            'style': '{',
        },
        'simple': {
            'format': '[{asctime}][{levelname}]: {message}',
            'datefmt' : '%d/%m/%Y %H:%M:%S',
            'style': '{',
        },
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'simple',
        },
        'file_server': {
            'class': 'logging.FileHandler',
            'filename': 'log/server.log',
            'formatter': 'simple',
        },
        'file_backend': {
            'class': 'logging.FileHandler',
            'filename': 'log/backend.log',
            'formatter': 'simple',
        },
        'file_provider': {
            'class': 'logging.FileHandler',
            'filename': 'log/provider.log',
            'formatter': 'simple',
        },
        'file_coordinator': {
            'class': 'logging.FileHandler',
            'filename': 'log/coordinator.log',
            'formatter': 'simple',
        },
        'file_interface': {
            'class': 'logging.FileHandler',
            'filename': 'log/interface.log',
            'formatter': 'simple',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['file_server'],
            'level': os.getenv('DJANGO_LOG_LEVEL', 'INFO'),
        },
        'daphne': {
            'handlers': ['file_server'],
            'level': os.getenv('DJANGO_LOG_LEVEL', 'INFO'),
        },
        'backend': {
            'handlers': ['file_backend'],
            'level': os.getenv('BACKEND_LOG_LEVEL', 'INFO'),
        },
        'provider': {
            'handlers': ['file_provider'],
            'level': os.getenv('PROVIDER_LOG_LEVEL', 'INFO'),
        },
        'interface': {
            'handlers': ['file_interface'],
            'level': os.getenv('INTERFACE_LOG_LEVEL', 'INFO'),
        },
        'coordinator': {
            'handlers': ['file_coordinator', 'console'],
            'level': os.getenv('COORDINATOR_LOG_LEVEL', 'INFO'),
        },
    },
}

CHANNEL_LAYERS = {
    'default': {
        'BACKEND': 'channels_redis.core.RedisChannelLayer',
        'CONFIG': {
            "hosts": ["redis://:" + os.getenv('REDIS_PASSWORD', '') + "@127.0.0.1:6379/0"],
        },
    },
}

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.1/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = '/www/provider/static/'

MEDIA_URL = '/media/'
MEDIA_ROOT = '/www/provider/media/'

DEFAULT_DATASET_FOLDER = '/www/provider/dataset/'

if not os.path.exists(DEFAULT_DATASET_FOLDER):
    os.makedirs(DEFAULT_DATASET_FOLDER)
if not os.path.exists('log/'):
    os.makedirs('log/')
