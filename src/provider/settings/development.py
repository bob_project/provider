"""
La configuration à utiliser pour les environnements de dévelopement
"""

from provider.settings.base import *

from dotenv import load_dotenv

load_dotenv(verbose=True, override=True, dotenv_path='.env')

SECRET_KEY = 'uehfj@070q!sexgtk4&5r6hho&*2&1j9vjyk#jjxe22m2=(3(y'

DEBUG = True

INSTALLED_APPS.append('debug_toolbar')
MIDDLEWARE.insert(0, 'debug_toolbar.middleware.DebugToolbarMiddleware')

ALLOWED_HOSTS = ['127.0.0.1', 'localhost']
INTERNAL_IPS = ["127.0.0.1"]

CORS_ALLOW_ALL_ORIGINS = True

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'simple': {
            'format': '[{asctime}][{levelname}]: {message}',
            'datefmt' : '%d/%m/%Y %H:%M:%S',
            'style': '{',
        },
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'simple',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'level': os.getenv('DJANGO_LOG_LEVEL', 'INFO'),
        },
        'backend': {
            'handlers': ['console'],
            'level': os.getenv('BACKEND_LOG_LEVEL', 'INFO'),
        },
        'provider': {
            'handlers': ['console'],
            'level': os.getenv('PROVIDER_LOG_LEVEL', 'INFO'),
        },
        'coordinator': {
            'handlers': ['console'],
            'level': os.getenv('COORDINATOR_LOG_LEVEL', 'INFO'),
        },
        'interface': {
            'handlers': ['console'],
            'level': os.getenv('INTERFACE_LOG_LEVEL', 'INFO'),
        },
    },
}

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3')
    }
}

CHANNEL_LAYERS = {
    'default': {
        'BACKEND': 'channels_redis.core.RedisChannelLayer',
        'CONFIG': {
            "hosts": [('127.0.0.1', 6379)],
        },
    },
}


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.1/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static/')

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media/')

DEFAULT_DATASET_FOLDER = '../dataset/'

if not os.path.exists(DEFAULT_DATASET_FOLDER):
    os.makedirs(DEFAULT_DATASET_FOLDER)
