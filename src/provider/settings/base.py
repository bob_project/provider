"""
La configuration de base pour tous les environnements
"""

import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Application definition

INSTALLED_APPS = [
    'channels',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'corsheaders',
    'provider_backend',
    'provider_coordinator',
    'provider_interface',
]

MIDDLEWARE = [
    'django.middleware.common.CommonMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'provider_backend.middlewares.WorkerHTTPMiddleware',
]

ROOT_URLCONF = 'provider.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

ASGI_APPLICATION = 'provider.asgi.application'
WSGI_APPLICATION = 'provider.wsgi.application'

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'simple': {
            'format': '[{asctime}][{levelname}]: {message}',
            'datefmt' : '%d/%m/%Y %H:%M:%S',
            'style': '{',
        },
        'verbose': {
            'format': '[{asctime}][{levelname}]: {module} {process:d} {thread:d} {message}',
            'datefmt' : '%d/%m/%Y %H:%M:%S',
            'style': '{',
        },
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'simple',
        },
        'file_server': {
            'class': 'logging.FileHandler',
            'filename': 'server.log',
            'formatter': 'verbose',
        },
        'file_backend': {
            'class': 'logging.FileHandler',
            'filename': 'backend.log',
            'formatter': 'verbose',
        },
        'file_provider': {
            'class': 'logging.FileHandler',
            'filename': 'provider.log',
            'formatter': 'verbose',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console', 'file_server'],
            'level': os.getenv('DJANGO_LOG_LEVEL', 'INFO'),
        },
        'backend': {
            'handlers': ['console', 'file_backend'],
            'level': os.getenv('BACKEND_LOG_LEVEL', 'INFO'),
        },
        'provider': {
            'handlers': ['console', 'file_provider'],
            'level': os.getenv('PROVIDER_LOG_LEVEL', 'INFO'),
        },
    },
}

# Password validation
# https://docs.djangoproject.com/en/3.1/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Internationalization
# https://docs.djangoproject.com/en/3.1/topics/i18n/

LANGUAGE_CODE = 'fr-FR'

TIME_ZONE = 'Europe/Paris'

USE_I18N = True

USE_L10N = True

USE_TZ = True

WORKER_CODE = "/static/wasm_worker.js"

ATTRIBUTORS = {
    "FIFO": "provider_backend.scheduling.attributors.FIFOAttributor",
    "SPOTCK": "provider_backend.scheduling.attributors.SpotCheckAttributor",
}

PROCESSORS = {
    "SIMPLE": "provider_backend.scheduling.processors.SimpleProcessor",
}

VOTERS = {
    "MAJORITY": "provider_backend.scheduling.voters.MajorityVoter",
    "MFIRST": "provider_backend.scheduling.voters.MFirstVoter",
    "CREDIB": "provider_backend.scheduling.voters.CredibilityVoter",
}
