"""
La configuration à utiliser pour les environnements de test
"""

from provider.settings.base import *

DEBUG = True

SECRET_KEY = 'uehfj@070q!sexgtk4&5r6hho&*2&1j9vjyk#jjxe22m2=(3(y'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'provider_ci',
        'USER': 'postgres',
	    'PASSWORD': '',
        'HOST': 'postgres',
        'PORT': '5432',
    }
}

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.1/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static/')

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media/')

DEFAULT_DATASET_FOLDER = '../dataset/'

if not os.path.exists(DEFAULT_DATASET_FOLDER):
    os.makedirs(DEFAULT_DATASET_FOLDER)
