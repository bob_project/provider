"""
This module describe all WebSocket routes of the project
"""

from django.urls import path

from provider_backend.consumers import BackendConsumer
from provider_coordinator.consumers import CoordinatorConsumer

websocket_urlpatterns = [
    path('backend/', BackendConsumer.as_asgi()),
    path('coordinator/', CoordinatorConsumer.as_asgi()),
]
